function alertaEsqueceu(link) {

	alert("Foi enviado um e-mail para informações de acesso ao SGP.");
	window.location = link;

}

function alertaEsqueceuFechar(msg) {

	alert(msg);
	window.close();

}

$(document).ready(
		function() {
			PrimeFaces.locales['pt_BR'] = {
				closeText : 'Fechar',
				prevText : 'Anterior',
				nextText : 'Pr\u00F3ximo',
				currentText : 'Come\u00E7o',
				monthNames : [ 'Janeiro', 'Fevereiro', 'Mar\u00E7o', 'Abril',
						'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro',
						'Outubro', 'Novembro', 'Dezembro' ],
				monthNamesShort : [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
						'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
				dayNames : [ 'Domingo', 'Segunda', 'Ter\u00E7a', 'Quarta',
						'Quinta', 'Sexta', 'S\u00E1bado' ],
				dayNamesShort : [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex',
						'Sáb' ],
				dayNamesMin : [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
				weekHeader : 'Semana',
				firstDay : 1,
				isRTL : false,
				showMonthAfterYear : false,
				yearSuffix : '',
				timeOnlyTitle : 'S\u00F3 Horas',
				timeText : 'Tempo',
				hourText : 'Hora',
				minuteText : 'Minuto',
				secondText : 'Segundo',
				currentText : 'Data Atual',
				ampm : false,
				month : 'M\u00EAs',
				week : 'Semana',
				day : 'Dia',
				allDayText : 'Todo Dia'
			};
		});


function alteraMaiusculo() {
	$(":input").keyup(function() {

		var start = this.selectionStart, end = this.selectionEnd;

		$(this).val($(this).val().toUpperCase());
		this.setSelectionRange(start, end);
	});
}

var l = document.getElementsByClassName('zeros');
setInterval(function() {
	for (var i = 0; i < l.length; i++) {
		while (l[i].value.length > 1 && l[i].value.substring(0, 1) == '0'
				&& l[i].value.substring(0, 2) != '0.') {
			var s = l[i].selectionStart;
			l[i].value = l[i].value.substring(1);
			l[i].selectionEnd = l[i].selectionStart = s - 1;
		}
	}
}, 75);


$(document).ready(function() {

	   $('.ui-menuitem-link').each(function(){
	       if(window.location.pathname.indexOf($(this).attr('href')) != -1) {
	           $(this).css('background', '#E8E8E8');//or add class
	       }
	   });  

	})
	
	
	
	
function popupwindow(url, title) {      
    window.open(url , title, "toolbar=no, scrollbars=yes, resizable=yes, top=170, left=170, width=800, height=600");        
} 

//accordion ajuda
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
  }
}

function findCEP() {

	var cep = ($.trim($("#cepImovel").val()).replace(/[^\d]+/g,''));

	if(cep != ""){
    	 $.getScript("http://cep.republicavirtual.com.br/web_cep.php?cep= "+cep+"&formato=javascript", function(){
            if(resultadoCEP["resultado"] == 1){
            	$("#logradouroImovel").val(unescape(resultadoCEP["tipo_logradouro"]).toUpperCase()+" "+unescape(resultadoCEP["logradouro"].toUpperCase()));
             //$("#tipo_local").val(unescape(resultadoCEP["tipo_logradouro"]));
               $("#bairroImovel").val(unescape(resultadoCEP["bairro"].toUpperCase()));
                $("#cidadeImovel").val(unescape(resultadoCEP["cidade"].toUpperCase()));
               //$("select[id='estadoEndereco'] option:selected").val(unescape(resultadoCEP["uf"].toUpperCase()));
                //$("#estadoEndereco_label").val(unescape(resultadoCEP["uf"].toUpperCase()));
                 document.getElementById("estadoEndereco_label").innerHTML = unescape(resultadoCEP["uf"].toUpperCase());
                $("#estadoEndereco_input").val(unescape(resultadoCEP["uf"].toUpperCase()));
                document.getElementById("numeroImovel").focus();
               
          }else{
                alert("Endereço não encontrado para o cep ");
           }
        });
    }
}


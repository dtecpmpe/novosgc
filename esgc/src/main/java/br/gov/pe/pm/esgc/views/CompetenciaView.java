package br.gov.pe.pm.esgc.views;

import java.util.Date;

import br.gov.pe.pm.esgc.model.Conta;
import br.gov.pe.pm.esgc.model.Imovel;
import br.gov.pe.pm.esgc.model.TipoConta;
import br.gov.pe.pm.esgc.model.TipoFatura;

public class CompetenciaView {

	private Integer idCompetencia;
	private Conta conta;
	private Date competencia;
	private Date vencimento;
	private Double consumo;
	private Double esgoto;
	private Double valor;
	private TipoFatura tipoFatura;
	private TipoConta tipoConta;
	private Imovel imovel;
	
	public Integer getIdCompetencia() {
		return idCompetencia;
	}
	public void setIdCompetencia(Integer idCompetencia) {
		this.idCompetencia = idCompetencia;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public Date getCompetencia() {
		return competencia;
	}
	public void setCompetencia(Date competencia) {
		this.competencia = competencia;
	}
	public Date getVencimento() {
		return vencimento;
	}
	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}
	public Double getConsumo() {
		return consumo;
	}
	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}
	public Double getEsgoto() {
		return esgoto;
	}
	public void setEsgoto(Double esgoto) {
		this.esgoto = esgoto;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public TipoFatura getTipoFatura() {
		return tipoFatura;
	}
	public void setTipoFatura(TipoFatura tipoFatura) {
		this.tipoFatura = tipoFatura;
	}
	public TipoConta getTipoConta() {
		return tipoConta;
	}
	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}
	public Imovel getImovel() {
		return imovel;
	}
	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}
	
}

package br.gov.pe.pm.esgc.rn;

import java.util.Date;
import java.util.List;

import br.gov.pe.pm.esgc.hibernateDAO.CompetenciaHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.CompetenciaDAO;
import br.gov.pe.pm.esgc.model.Competencia;
import br.gov.pe.pm.esgc.model.Conta;
import br.gov.pe.pm.esgc.model.TipoConta;
import br.gov.pe.pm.esgc.util.db.DAOFactory;


public class CompetenciaRn {


	private LogRn logRn = new LogRn();
	private CompetenciaDAO competenciaDAO = new CompetenciaHibernateDAO();
	
	public CompetenciaRn() {
		this.competenciaDAO = (CompetenciaDAO) DAOFactory.criarDAO(this.competenciaDAO);
	}
	
	public List<Competencia> listar() {
		return this.competenciaDAO.listar();	
	}	
		
	public void excluir(Competencia objetoAntigo) {
		this.competenciaDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}
	
	public Competencia salvar(Competencia objetoAntigo, Competencia competenciaNovo) {
		Competencia objetoNovo = this.competenciaDAO.salvar(competenciaNovo);		
		if (objetoNovo.getIdCompetencia() != null)
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
		else
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(),objetoNovo.toString());
		return objetoNovo;
	}
		
	public Competencia carregar(Integer idCompetencia) {
		// TODO Auto-generated method stub
		return this.competenciaDAO.carregar(idCompetencia);
	}
	
	public List<Competencia> listar(TipoConta tipoConta) {
		return this.competenciaDAO.listar(tipoConta);
	}
	
	public List<Competencia> listar(TipoConta tipoConta, Date competencia) {
		return this.competenciaDAO.listar(tipoConta, competencia);
	}

	public List<Competencia> listar(TipoConta tipoConta, Date competencia, Conta conta) {
		return this.competenciaDAO.listar(tipoConta, competencia, conta);
	}
	
}


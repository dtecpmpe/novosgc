package br.gov.pe.pm.esgc.util.converter;
/*
 * package br.gov.pe.pm.sgc.util.converter;
 * 
 * 
 * import java.util.List;
 * 
 * import javax.faces.application.FacesMessage; import
 * javax.faces.component.UIComponent; import javax.faces.context.FacesContext;
 * import javax.faces.convert.Converter; import
 * javax.faces.convert.ConverterException; import
 * javax.faces.convert.FacesConverter;
 * 
 * import br.gov.pe.pm.sgc.model.DadosContas; import
 * br.gov.pe.pm.sgc.rn.DadosContasRn;
 * 
 * @FacesConverter(forClass = DadosContas.class, value =
 * "autoCompleteDadosConverter") public class DadosContasConverter implements
 * Converter {
 * 
 * @Override public Object getAsObject(FacesContext context, UIComponent
 * component, String value) throws ConverterException { if (!value.equals(null)
 * && !value.equals("")) { List<DadosContas> dados; dados = new
 * DadosContasRn().listarPorMatricula(Integer.parseInt(value));
 * if(!dados.isEmpty()) { return dados.get(0); }else{ return null; }
 * 
 * } return null;
 * 
 * }
 * 
 * @Override public String getAsString(FacesContext context, UIComponent
 * component, Object value) throws ConverterException{ DadosContas dados =
 * (DadosContas) value; if(!dados.equals(null)) { return
 * String.valueOf(dados.getnContratoMatricula()); } return null; }
 * 
 * 
 * }
 */
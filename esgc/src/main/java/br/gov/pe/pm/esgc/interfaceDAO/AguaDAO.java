package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.Agua;

public interface AguaDAO extends Dao {

	public List<Agua> listar();
	
	public List<Agua> listarAnual();
	
	/*public Agua salvar(Agua agua);
	
	public void excluir(Agua agua);
	
	public List<Agua> listarAtivo();
	
	public Agua carregar(Integer id);*/

}


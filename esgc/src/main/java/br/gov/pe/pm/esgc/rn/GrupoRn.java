package br.gov.pe.pm.esgc.rn;

import java.util.List;

import br.gov.pe.pm.esgc.hibernateDAO.GrupoHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.GrupoDAO;
import br.gov.pe.pm.esgc.model.Grupo;
import br.gov.pe.pm.esgc.model.TipoConta;
import br.gov.pe.pm.esgc.rn.LogRn;
import br.gov.pe.pm.esgc.util.db.DAOFactory;

public class GrupoRn {

	private LogRn logRn = new LogRn();
	private GrupoDAO grupoDAO = new GrupoHibernateDAO();
	
	public GrupoRn() {
		this.grupoDAO = (GrupoDAO) DAOFactory.criarDAO(this.grupoDAO);
	}
	
	public List<Grupo> listar() {
		return this.grupoDAO.listar();
	
	}
	public List<Grupo> listarAtivo() {
		return this.grupoDAO.listarAtivo();
	}
	
	public List<Grupo> listarAtivoConta(TipoConta tipoConta) {
		return this.grupoDAO.listarAtivoConta(tipoConta);
	}
		
	public void excluir(Grupo objetoAntigo) {
		this.grupoDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}
	
	public Grupo salvar(Grupo objetoAntigo, Grupo grupoNovo) {
		Grupo objetoNovo = this.grupoDAO.salvar(grupoNovo);
		if (objetoNovo.getIdGrupo() == null)
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
		else
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(),objetoNovo.toString());
		return objetoNovo;
	}	

	public Object carregar(Integer idGrupo) {
		// TODO Auto-generated method stub
		return this.grupoDAO.carregar(idGrupo);
	}
	public List<Grupo> listarGrupo(Integer tipoConta) {
		return this.grupoDAO.listarGrupo(tipoConta);
	
	}	
}

package br.gov.pe.pm.esgc.rn;

import java.util.List;

import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.esgc.hibernateDAO.OrganizacaoEnderecoHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.OrganizacaoEnderecoDAO;
import br.gov.pe.pm.esgc.model.Imovel;

import br.gov.pe.pm.esgc.model.OrganizacaoEndereco;
import br.gov.pe.pm.esgc.rn.LogRn;

import br.gov.pe.pm.esgc.util.db.DAOFactory;

public class OrganizacaoEnderecoRn {
	
	private OrganizacaoEnderecoDAO organizacaoEnderecoDAO = new OrganizacaoEnderecoHibernateDAO();
	private LogRn logRn = new LogRn();
	//private Object imovelDAO;	
	
	
	public OrganizacaoEnderecoRn() {
		this.organizacaoEnderecoDAO = (OrganizacaoEnderecoDAO) DAOFactory.criarDAO(this.organizacaoEnderecoDAO);
	}	
	
	
	public List<OrganizacaoEndereco> listarPorOme(Integer organizacao){
		return this.organizacaoEnderecoDAO.listarPorOme(organizacao);
	}
	
	public List<OrganizacaoEndereco> listar(){
		return this.organizacaoEnderecoDAO.listar();
	}
		
	
	public OrganizacaoEndereco salvar(OrganizacaoEndereco objetoAntigo, OrganizacaoEndereco organizacaoEnderecoNovo) {
		OrganizacaoEndereco objetoNovo = this.organizacaoEnderecoDAO.salvar(organizacaoEnderecoNovo);
		if (objetoNovo.getIdOrganizacaoEndereco() != null)
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
		else
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), objetoNovo.toString());			
		return objetoNovo;
	}

	public void excluir(OrganizacaoEndereco objetoAntigo) {
		this.organizacaoEnderecoDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");			
	}

	
}

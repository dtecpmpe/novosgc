package br.gov.pe.pm.esgc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.gov.pe.pm.esgc.entity.entityview.OmeView;
import br.gov.pe.pm.esgc.model.Conta;
import br.gov.pe.pm.esgc.model.Grupo;
import br.gov.pe.pm.esgc.model.Imovel;
import br.gov.pe.pm.esgc.rn.GrupoRn;
import br.gov.pe.pm.esgc.rn.ImovelRn;
import br.gov.pe.pm.esgc.rn.OmeRn;
import br.gov.pe.pm.esgc.util.method.MetodosUtil;

@Named("cadastroImovelBean")
@ViewScoped
public class CadastroImovelBean implements Serializable {

	// variaveis
	private static final long serialVersionUID = -7525641308666746011L;
	private List<SelectItem> listaOme;
	private List<Imovel> listaImovel;
	private List<Imovel> listarImoveis;
	private OmeView omeView = new OmeView();
	private Imovel imovel = new Imovel();
	private Conta celpe = new Conta();
	private Conta compesa = new Conta();
	private Imovel imovelAnterior = new Imovel();
	private Conta contaAnterior = new Conta();

	// Getters and Setters
	public List<SelectItem> getListaOme() {
		return listaOme;
	}

	public void setListaOme(List<SelectItem> listaOme) {
		this.listaOme = listaOme;
	}

	public List<Imovel> getListaImovel() {
		return listaImovel;
	}

	public void setListaImovel(List<Imovel> listaImovel) {
		this.listaImovel = listaImovel;
	}

	public OmeView getOmeView() {
		return omeView;
	}

	public void setOmeView(OmeView omeView) {
		this.omeView = omeView;
	}

	public Imovel getImovel() {
		return imovel;
	}
	
	public List<Imovel> getListarImoveis() {
		return listarImoveis;
	}

	public void setListarImoveis(List<Imovel> listaImoveis) {
		this.listarImoveis = listaImoveis;
	}

	public void setImovel(Imovel imovel) {
		this.imovelAnterior = imovel.clone();
		this.imovel = imovel;
	}

	public Conta getCelpe() {
		return celpe;
	}

	public void setCelpe(Conta celpe) {
		this.celpe = celpe;
	}

	public Conta getCompesa() {
		return compesa;
	}

	public void setCompesa(Conta compesa) {
		this.compesa = compesa;
	}

	public Imovel getImovelAnterior() {
		return imovelAnterior;
	}

	public void setImovelAnterior(Imovel imovelAnterior) {
		this.imovelAnterior = imovelAnterior;
	}

	public Conta getContaAnterior() {
		return contaAnterior;
	}

	public void setContaAnterior(Conta contaAnterior) {
		this.contaAnterior = contaAnterior;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CadastroImovelBean() {
		omeView = new OmeView();
		
		/*
		 * if (this.imovel.getNome() == "SEDE" || this.imovel.getIdImovel() == null) {
		 * clone(); }
		 */
	}

	// Métodos

	public void listar() {
		this.setListaImovel(new ImovelRn().listar());
	}

	public void listarPorOme() {
		this.setListaImovel(new ImovelRn().listarPorOme(omeView.getIdOrganizacao()));
	}

	public String removeAcentos(String s) {
		return new MetodosUtil().removeAcentos(s);
	}

	public void novoImovel() {
		this.imovel = new Imovel();
		imovelAnterior = new Imovel();
	}

	public void salvarImovel() {

		this.imovel.setIdOme(omeView.getIdOrganizacao());
		this.imovel.setStatus(true);
		this.imovel = (Imovel) new ImovelRn().salvar(this.imovelAnterior, this.imovel);

		listarPorOme();
		// mensagem de sucesso
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext.getCurrentInstance().update(Arrays.asList("msgs", "tb_imovel"));
	}

	public String excluirImovel() {
		new ImovelRn().excluir(this.imovel);
		imovelAnterior = new Imovel();
		listarPorOme();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}

	public List<SelectItem> getListarOme() {
		if (this.listaOme == null) {
			this.listaOme = new ArrayList<SelectItem>();
			List<OmeView> pv = new OmeRn().listarOme();
			this.listaOme.add(new SelectItem(null, "Selecione..."));
			for (OmeView p : pv) {
				this.listaOme.add(new SelectItem(p.getIdOrganizacao(), p.getSigla()));
			}
		}
		return listaOme;
	}

	public void carregarDados() {
		omeView = new OmeRn().carregar(omeView.getIdOrganizacao());
		listarPorOme();
	}

	public List<SelectItem> getListarCelpe() {
		List<SelectItem> listaGrupo = new ArrayList<SelectItem>();
		List<Grupo> grupo = new GrupoRn().listarGrupo(1);
		listaGrupo.add(new SelectItem(null, "Selecione..."));
		for (Grupo g : grupo) {
			listaGrupo.add(new SelectItem(g.getGrupo()));
		}
		return listaGrupo;
	}

	public List<SelectItem> getListarCompesa() {
		List<SelectItem> listaGrupo = new ArrayList<SelectItem>();
		List<Grupo> grupo = new GrupoRn().listarGrupo(2);
		listaGrupo.add(new SelectItem(null, "Selecione..."));
		for (Grupo g : grupo) {
			listaGrupo.add(new SelectItem(g.getGrupo()));
		}
		return listaGrupo;
	}

	public String editar(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", id);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("call", "1");
		return "cadastro_contas?faces-redirect=true";
	}

	public String redireciona(String imovel) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("imov", imovel);
		return "cadastro_contas?faces-redirect=true";
	}

	@Override
	public Imovel clone() {
		if (this.imovel.getIdImovel() == null|| this.imovel.getNome() == "SEDE") {
			Imovel novo = new Imovel();
			novo.setNome("SEDE");
			novo.setBairro(omeView.getBairro());
			novo.setCep(omeView.getCep());
			novo.setCidade(omeView.getCidade());
			novo.setComplemento(omeView.getComplemento());
			// novo.setIdImovel(omeView.getIdOrganizacao());
			novo.setIdOme(omeView.getIdOrganizacao());
			novo.setLogradouro(omeView.getLogradouro());
			novo.setNumero(omeView.getNumero());
			novo.setPontoReferencia(omeView.getPontoReferencia());
			//novo.setStatus(omeView.getAtivo());
			novo.setTelefone(omeView.getTelefone());
			// novo.setBairro(omeView.getBairro());
			// E assim por diante para todas as propriedades.
			salvarImovel();
		}
		return null;
		
	}
	
	public void listarImoveis() {
		this.setListarImoveis(new ImovelRn().listar());
	}

}

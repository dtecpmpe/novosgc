package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.gov.pe.pm.esgc.entity.Log;
import br.gov.pe.pm.esgc.interfaceDAO.LogDAO;

public class LogHibernateDAO implements LogDAO {
	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	public Log salvar(Log log){
		return (Log) this.session.merge(log);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Log> listar() {

		return this.session.createCriteria(Log.class).addOrder(Order.desc("dataAlteracao")).list();
	}
}

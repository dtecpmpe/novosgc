/*
 * package br.gov.pe.pm.esgc.controller;
 * 
 * import java.io.Serializable; import java.util.ArrayList; import
 * java.util.Arrays; import java.util.EnumSet; import java.util.List;
 * 
 * import javax.faces.application.FacesMessage; import
 * javax.faces.context.FacesContext; import javax.faces.model.SelectItem; import
 * javax.faces.view.ViewScoped; import javax.inject.Named;
 * 
 * import org.primefaces.context.RequestContext; import
 * org.springframework.security.core.context.SecurityContext; import
 * org.springframework.security.core.context.SecurityContextHolder;
 * 
 * import br.gov.pe.pm.esgc.entity.Pessoa; import
 * br.gov.pe.pm.esgc.entity.Usuario; import
 * br.gov.pe.pm.esgc.entity.entityview.OmeView; import
 * br.gov.pe.pm.esgc.entity.enumAux.PerfilEnum; import
 * br.gov.pe.pm.esgc.rn.OmeRn;
 * 
 * import br.gov.pe.pm.esgc.rn.UsuarioRn; import
 * br.gov.pe.pm.esgc.util.method.MetodosUtil;
 * 
 * @Named("usuario1Bean")
 * 
 * @ViewScoped public class Usuario1Bean implements Serializable {
 * 
 * private static final long serialVersionUID = 2843369974669422035L; //
 * variáveis private List<Usuario> listaUsuarios; private List<SelectItem>
 * listaPerfils; private List<SelectItem> listaOrganizacao; private Usuario
 * usuario = new Usuario(); private Usuario usuarioAnterior = new Usuario();
 * private String novaSenha; private String senhaAnterior; private String
 * confirmarSenha; private String cpfAux;
 * 
 * // getters e setters public List<Usuario> getListaUsuarios() { return
 * listaUsuarios; }
 * 
 * public void setListaUsuarios(List<Usuario> listaUsuarios) {
 * this.listaUsuarios = listaUsuarios; }
 * 
 * public Usuario getUsuario() { return usuario; }
 * 
 * public void setUsuario(Usuario usuario) { this.usuarioAnterior =
 * usuario.clone(); this.usuario = usuario; }
 * 
 * public String getNovaSenha() { return novaSenha; }
 * 
 * public void setNovaSenha(String novaSenha) { this.novaSenha = novaSenha; }
 * 
 * public String getSenhaAnterior() { return senhaAnterior; }
 * 
 * public void setSenhaAnterior(String senhaAnterior) { this.senhaAnterior =
 * senhaAnterior; }
 * 
 * public String getConfirmarSenha() { return confirmarSenha; }
 * 
 * public void setConfirmarSenha(String confirmarSenha) { this.confirmarSenha =
 * confirmarSenha; }
 * 
 * public List<SelectItem> getPerfils() { if (this.listaPerfils == null) {
 * this.listaPerfils = new ArrayList<SelectItem>(); List<PerfilEnum> perfil =
 * new ArrayList<PerfilEnum>(EnumSet.allOf(PerfilEnum.class));
 * this.listaPerfils.add(new SelectItem(null, "Selecione...")); for (PerfilEnum
 * p : perfil) { this.listaPerfils.add(new SelectItem(p.name(),
 * p.getDescricao())); } } return listaPerfils; }
 * 
 * public List<SelectItem> getListaOrganizacao() { if (this.listaOrganizacao ==
 * null) { this.listaOrganizacao = new ArrayList<SelectItem>(); List<OmeView>
 * org = new OmeRn().listarOme(); this.listaOrganizacao.add(new SelectItem(null,
 * "Selecione...")); for (OmeView o : org) { this.listaOrganizacao.add(new
 * SelectItem(o.getIdOrganizacao(), o.getSigla())); } } return listaOrganizacao;
 * }
 * 
 * public void setListaOrganizacao(List<SelectItem> listaOrganizacao) {
 * this.listaOrganizacao = listaOrganizacao; }
 * 
 * public String getCpfAux() { return cpfAux; }
 * 
 * public void setCpfAux(String cpfAux) { this.cpfAux = cpfAux; }
 * 
 * // métodos public void listar() { this.setListaUsuarios(new
 * UsuarioRn().listar()); }
 * 
 * 
 * public void carregarUsuario() { this.setUsuario(new
 * UsuarioRn().buscarPorLogin((((SecurityContext)
 * SecurityContextHolder.getContext()).getAuthentication().getName()))); }
 * 
 * public String removeAcentos(String s) { return new
 * MetodosUtil().removeAcentos(s); }
 * 
 * public void novo() { this.usuario = new Usuario(); usuarioAnterior = new
 * Usuario(); System.out.println("########"); }
 * 
 * public void salvar() {
 * 
 * String mensagem = "Usuário salvo com sucesso! "; if
 * (this.usuario.getIdUsuario() == null) { System.out.println("aaaaaaaaa");
 * this.usuario.setAtivo(true); this.usuario.setOrganizacao(8); this.usuario =
 * (Usuario) new UsuarioRn().salvar(this.usuarioAnterior, this.usuario);
 * System.out.println("bbbbbbbbbbb"); mensagem +=
 * "A senha inicial é: PMPE1825."; }
 * 
 * new UsuarioRn().salvar(this.usuarioAnterior, this.usuario); listar();
 * FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem,
 * null); FacesContext.getCurrentInstance().addMessage(null, message);
 * RequestContext.getCurrentInstance().update(Arrays.asList("frm:msgs",
 * "frm:cons_usuario"));
 * 
 * }
 * 
 * 
 * 
 * public String excluir() { UsuarioRn usuarioRn = new UsuarioRn();
 * usuarioRn.excluir(this.usuario); usuarioAnterior = new Usuario(); listar();
 * FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
 * "Usuário excluído com sucesso!", null);
 * FacesContext.getCurrentInstance().addMessage(null, message);
 * 
 * return null; }
 * 
 * public String alterarSenha() {
 * 
 * FacesContext context = FacesContext.getCurrentInstance(); FacesMessage msg;
 * 
 * if (this.usuario.getSenha().equals(new
 * MetodosUtil().transformarMd5(senhaAnterior))) {
 * 
 * if (this.novaSenha.equals(this.confirmarSenha)) {
 * 
 * UsuarioRn usuarioRn = new UsuarioRn(); this.usuario.setSenha(new
 * MetodosUtil().transformarMd5(this.confirmarSenha));
 * usuarioRn.salvar(this.usuarioAnterior, this.usuario);
 * 
 * msg = new FacesMessage("A senha foi alterada!");
 * msg.setSeverity(FacesMessage.SEVERITY_INFO); context.addMessage(null, msg);
 * 
 * } else {
 * 
 * msg = new FacesMessage("A nova senha não foi confirmada corretamente!");
 * msg.setSeverity(FacesMessage.SEVERITY_ERROR); context.addMessage(null, msg);
 * 
 * }
 * 
 * } else { msg = new FacesMessage("A senha atual não confere!");
 * msg.setSeverity(FacesMessage.SEVERITY_ERROR); context.addMessage(null, msg);
 * }
 * 
 * this.novaSenha = null; this.senhaAnterior = null; this.confirmarSenha = null;
 * 
 * return null;
 * 
 * }
 * 
 * public void resetarSenha() {
 * 
 * String senha = "PMPE1825"; this.usuario.setSenha(new
 * MetodosUtil().transformarMd5(senha)); new
 * UsuarioRn().salvar(this.usuarioAnterior, this.usuario); listar();
 * FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
 * "A nova senha para este usuário é: " + senha, null);
 * FacesContext.getCurrentInstance().addMessage(null, message);
 * RequestContext.getCurrentInstance().update(Arrays.asList("frm:msgs",
 * "frm:cons_usuario"));
 * 
 * }
 * 
 * 
 * public List<String> complete(String numero) { List<String> results = new
 * ArrayList<String>(); List<Pessoa> pes = new PessoaRn().listarPorCpf(numero);
 * for (Pessoa p : pes) { String num = p.getCpf(); results.add(num); } return
 * results; }
 * 
 * 
 * 
 * public void carregarPorCpf() {
 * 
 * System.out.
 * println("ENRTOU + ===================================================");
 * 
 * }
 * 
 * 
 * 
 * public void carregarPorCpf() { Pessoa p = new
 * PessoaRn().carregarPorCpf(this.usuario.getLogin()); if (p != null) { Usuario
 * u = new UsuarioRn().buscarPorLogin(this.usuario.getLogin());
 * 
 * if (u == null || u.equals(null)) { this.usuario.setAtivo(true);
 * this.usuario.setNome(p.getNomeCompleto());
 * this.usuario.setOrganizacao(8);//new
 * OmeRn().carregar(p.getPessoaSituacaoAtual().getIdOrganizacaoDisp())); } else
 * { novo(); FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN,
 * "CPF já cadastrado como usuário!", null);
 * FacesContext.getCurrentInstance().addMessage(null, message);
 * FacesContext.getCurrentInstance().validationFailed(); }
 * 
 * } else { novo(); FacesMessage message = new
 * FacesMessage(FacesMessage.SEVERITY_WARN,
 * "CPF não encontrado na base de pessoas!", null);
 * FacesContext.getCurrentInstance().addMessage(null, message);
 * FacesContext.getCurrentInstance().validationFailed(); }
 * 
 * }
 * 
 * 
 * }
 */
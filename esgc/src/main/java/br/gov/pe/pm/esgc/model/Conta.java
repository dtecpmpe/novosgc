package br.gov.pe.pm.esgc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="conta", schema = "conta")
public class  Conta  implements Serializable, Cloneable {
	
	
	private static final long serialVersionUID = -8244744816916503795L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id_conta", nullable = false )
	private Integer idConta;
	
	@Column
	private String nContratoMatricula;
	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_imovel", foreignKey = @ForeignKey(name = "conta_id_imovel_fkey") )
	private Imovel imovel;
	
	@Column (name= "hidrometro_medidor")
	private String hidrometroMedidor;
	
	@Column
	private String observacao;
		
	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipo_conta", foreignKey = @ForeignKey(name = "conta_id_tipo_conta_fkey") )
	private TipoConta tipoConta;
	
	@Column (name="data_inicial")
	private Date dataInicio;
	
	@Column  (name="data_final")
	private Date dataFinal;
	
	@Column
	private Boolean status;
	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_grupo", foreignKey = @ForeignKey(name = "conta_id_grupo_fkey") )
	private Grupo grupo;
	
	@Column
	private String demanda;
	
	
	
	public Conta() {
		this.tipoConta = new TipoConta();
		this.imovel =new Imovel();
		this.grupo = new Grupo();
	}

	public Integer getIdConta() {
		return idConta;
	}

	public void setIdConta(Integer idConta) {
		this.idConta = idConta;
	}

	public String getnContratoMatricula() {
		return nContratoMatricula;
	}

	public void setnContratoMatricula(String nContratoMatricula) {
		this.nContratoMatricula = nContratoMatricula;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	public String getHidrometroMedidor() {
		return hidrometroMedidor;
	}

	public void setHidrometroMedidor(String hidrometroMedidor) {
		this.hidrometroMedidor = hidrometroMedidor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public String getDemanda() {
		return demanda;
	}

	public void setDemanda(String demanda) {
		this.demanda = demanda;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataFinal == null) ? 0 : dataFinal.hashCode());
		result = prime * result + ((dataInicio == null) ? 0 : dataInicio.hashCode());
		result = prime * result + ((demanda == null) ? 0 : demanda.hashCode());
		result = prime * result + ((grupo == null) ? 0 : grupo.hashCode());
		result = prime * result + ((hidrometroMedidor == null) ? 0 : hidrometroMedidor.hashCode());
		result = prime * result + ((idConta == null) ? 0 : idConta.hashCode());
		result = prime * result + ((imovel == null) ? 0 : imovel.hashCode());
		result = prime * result + ((nContratoMatricula == null) ? 0 : nContratoMatricula.hashCode());
		result = prime * result + ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tipoConta == null) ? 0 : tipoConta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conta other = (Conta) obj;
		if (dataFinal == null) {
			if (other.dataFinal != null)
				return false;
		} else if (!dataFinal.equals(other.dataFinal))
			return false;
		if (dataInicio == null) {
			if (other.dataInicio != null)
				return false;
		} else if (!dataInicio.equals(other.dataInicio))
			return false;
		if (demanda == null) {
			if (other.demanda != null)
				return false;
		} else if (!demanda.equals(other.demanda))
			return false;
		if (grupo == null) {
			if (other.grupo != null)
				return false;
		} else if (!grupo.equals(other.grupo))
			return false;
		if (hidrometroMedidor == null) {
			if (other.hidrometroMedidor != null)
				return false;
		} else if (!hidrometroMedidor.equals(other.hidrometroMedidor))
			return false;
		if (idConta == null) {
			if (other.idConta != null)
				return false;
		} else if (!idConta.equals(other.idConta))
			return false;
		if (imovel == null) {
			if (other.imovel != null)
				return false;
		} else if (!imovel.equals(other.imovel))
			return false;
		if (nContratoMatricula == null) {
			if (other.nContratoMatricula != null)
				return false;
		} else if (!nContratoMatricula.equals(other.nContratoMatricula))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (tipoConta == null) {
			if (other.tipoConta != null)
				return false;
		} else if (!tipoConta.equals(other.tipoConta))
			return false;
		return true;
	}

	@Override
	public Conta clone() {
		try {
			return (Conta)super.clone();
		} catch (CloneNotSupportedException e) {
			//System.out.println("O objeto CIDADE não pode ser clonado." + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public String toString() {
		return "idConta= " + idConta + ", nContratoMatricula= " + nContratoMatricula + ", imovel= " + imovel + 
				", hidrometroMedidor= " + hidrometroMedidor +  ", observaçao= " + observacao +  ", tipoConta= " + tipoConta +  
				", dataInicio= " + dataInicio +  ", dataFinal= " + dataFinal + ", status= " + status +  ", grupo= " + grupo + 
				", demanda= " + demanda + ".";
	}
}
package br.gov.pe.pm.esgc.util.converter;


import javax.faces.convert.FacesConverter;

import java.text.DateFormat;
import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

//@FacesConverter("competenciaConverter")
public class CompetenciaConverter implements Converter {
	
	

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorTela) throws ConverterException {
		if (valorTela == null || valorTela.toString().trim().equals("")) {
			return 0.0d;
		} else {
			try {
				return Double.parseDouble(valorTela.replace("/", "-"));
			} catch (Exception e) {
				return 0.0d;
			}
		}
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object valorTela) throws ConverterException {
		if (valorTela == null || valorTela.toString().trim().equals("")) {
			return "00/0000";
		} else {
			DateFormat nf = DateFormat.getDateInstance(0, new Locale("pt", "BR"));
			nf.format("MM/yyyy");
			return nf.format(Double.valueOf(valorTela.toString()));
		}
	}
}

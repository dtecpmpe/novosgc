package br.gov.pe.pm.esgc.model;



import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.gov.pe.pm.esgc.entity.entityview.OmeView;
import br.gov.pe.pm.esgc.rn.OmeRn;

@Entity
@Table(name = "organizacao_endereco", schema = "mrh")
public class OrganizacaoEndereco implements Serializable {


	private static final long serialVersionUID = -8594948315296520889L;

	// VARIÁVEIS
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_organizacao_endereco", nullable = false)
	private Integer idOrganizacaoEndereco;

	@Column(length = 100)
	private String bairro;
	
	@Column(length = 100)
	private String telefone;
	
	@Column(length = 100)
	private String nome;

	@Column(length = 10)
	private String cep;

	@Column(length = 100)
	private String cidade;

	@Column(length = 30)
	private String complemento;

	@Column(length = 2)
	private String estado;

	@Column(length = 100)
	private String logradouro;

	@Column(length = 10)
	private String numero;

	@Column(name = "ponto_referencia", length = 100)
	private String pontoReferencia;


	@Column(name = "id_organizacao")
	private Integer organizacao;

	// CONSTRUTOR
	public OrganizacaoEndereco() {
	}

	public Integer getIdOrganizacaoEndereco() {
		return idOrganizacaoEndereco;
	}

	public void setIdOrganizacaoEndereco(Integer idOrganizacaoEndereco) {
		this.idOrganizacaoEndereco = idOrganizacaoEndereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}
	
	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getLogradouro() {
		return logradouro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPontoReferencia() {
		return pontoReferencia;
	}

	public void setPontoReferencia(String pontoReferencia) {
		this.pontoReferencia = pontoReferencia;
	}

	public Integer getOrganizacao() {
		return organizacao;
	}

	public void setOrganizacao(Integer organizacao) {
		this.organizacao = organizacao;
	}

	@Override
	public String toString() {
		return "idOrganizacaoEndereco= " + idOrganizacaoEndereco + "nome = "+ nome + ", cep= " + cep + ", telefone = " + telefone + ", logradouro= " + logradouro + 
				", numero= " + numero + ", bairro= " + bairro + ", cidade= " + cidade + ", complemento= " +
				complemento + ", pontoReferencia= " + pontoReferencia+ ", idOme= "+
				 organizacao+  ".";
	}
	
	public String getOme() {
		return new OmeRn().carregar(organizacao).getSigla();
	}
}

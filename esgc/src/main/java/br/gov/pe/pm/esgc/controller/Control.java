package br.gov.pe.pm.esgc.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

@ManagedBean
public class Control implements Serializable {

	// Apenas para simplificar: carrega automaticamente a Entity através do DAO
	// private DocumentoPdf documentoPdf = new Dao().buscarDocumento();

	private static final long serialVersionUID = 988621322334456532L;

	public void visualizarPdf(byte[] documentoPdf) {

		FacesContext fc = FacesContext.getCurrentInstance();

		// Obtem o HttpServletResponse, objeto responsável pela resposta do
		// servidor ao browser
		HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();

		// Limpa o buffer do response
		response.reset();

		// Seta o tipo de conteudo no cabecalho da resposta. No caso, indica que
		// o
		// conteudo sera um documento pdf.
		response.setContentType("application/pdf");

		// Seta o tamanho do conteudo no cabecalho da resposta. No caso, o
		// tamanho
		// em bytes do pdf
		response.setContentLength(documentoPdf.length);

		// Seta o nome do arquivo e a disposição: "inline" abre no próprio
		// navegador
		// Mude para "attachment" para indicar que deve ser feito um download
		response.setHeader("Content-disposition", "inline; filename=arquivo.pdf");
		try {

			// Envia o conteudo do arquivo PDF para o response
			response.getOutputStream().write(documentoPdf);

			// Descarrega o conteudo do stream, forçando a escrita de qualquer
			// byte
			// ainda em buffer
			response.getOutputStream().flush();

			// Fecha o stream, liberando seus recursos
			response.getOutputStream().close();
			// Sinaliza ao JSF que a resposta HTTP para este pedido já foi
			// gerada
			fc.responseComplete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
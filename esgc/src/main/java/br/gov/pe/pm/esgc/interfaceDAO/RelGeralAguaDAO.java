package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.RelGeralAgua;

public interface RelGeralAguaDAO extends Dao {

	public List<RelGeralAgua> listar();
	
}


package br.gov.pe.pm.esgc.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;

import br.gov.pe.pm.esgc.model.RelGeralAgua;
import br.gov.pe.pm.esgc.model.RelGeralLuz;
import br.gov.pe.pm.esgc.rn.RelGeralAguaRn;
import br.gov.pe.pm.esgc.rn.RelGeralLuzRn;

@Named("relGeralBean")
@ViewScoped
public class RelGeralBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<RelGeralLuz>listaLuz;
	private List<RelGeralAgua>listaAgua;


	public void listar() {
		setListaLuz(new RelGeralLuzRn().listar());
		setListaAgua(new RelGeralAguaRn().listar());
	}

	public List<RelGeralLuz> getListaLuz() {
		return listaLuz;
	}

	public void setListaLuz(List<RelGeralLuz> listaLuz) {
		this.listaLuz = listaLuz;
	}

	public List<RelGeralAgua> getListaAgua() {
		return listaAgua;
	}

	public void setListaAgua(List<RelGeralAgua> listaAgua) {
		this.listaAgua = listaAgua;
	}
	
	public boolean reduziu(Integer id, String valor) {
		if(id!=null && valor!= null) {
		if(id.equals(3) && valor.startsWith("REDUZIU")) {
			return true;
		}else {
			return false;
		}
		}else {
			return false;
		}
	}
		
	public boolean aumentou(Integer id, String valor) {
		if(id!=null && valor!= null) {
		if(id.equals(3) && valor.startsWith("AUMENTOU")) {
			return true;
		}else {
			return false;
		}
		}else {
			return false;
		}
	}
	
	
	
	
	
	
	 private BarChartModel barModel;
	 private HorizontalBarChartModel horizontalBarModel;
	 private BarChartModel animatedModel2;
	
	@PostConstruct
    public void init() {
		listar();

       
        createBarModels();
      
        
    }
	
	public BarChartModel getAnimatedModel2() {
        return animatedModel2;
    }
	
	 private BarChartModel initBarModel() {
	        BarChartModel model = new BarChartModel();
	 
	        ChartSeries ano1 = new ChartSeries();
	        ano1.setLabel(listaLuz.get(0).getAno());
	        ano1.set("JAN", Double.parseDouble(listaLuz.get(0).getJaneiro().replaceAll( "," , "." )));
	        ano1.set("FEV", Double.parseDouble(listaLuz.get(0).getFevereiro().replaceAll( "," , "." )));
	        ano1.set("MAR", Double.parseDouble(listaLuz.get(0).getMarco().replaceAll( "," , "." )));
	        ano1.set("ABR", Double.parseDouble(listaLuz.get(0).getAbril().replaceAll( "," , "." )));
	        ano1.set("MAI", Double.parseDouble(listaLuz.get(0).getMaio().replaceAll( "," , "." )));
	 
	        ChartSeries ano2 = new ChartSeries();
	        ano2.setLabel(listaLuz.get(1).getAno());
	        ano2.set("JAN", Double.parseDouble(listaLuz.get(1).getJaneiro().replaceAll( "," , "." )));
	        ano2.set("FEV", Double.parseDouble(listaLuz.get(1).getFevereiro().replaceAll( "," , "." )));
	        ano2.set("MAR", Double.parseDouble(listaLuz.get(1).getMarco().replaceAll( "," , "." )));
	        ano2.set("ABR", Double.parseDouble(listaLuz.get(1).getAbril().replaceAll( "," , "." )));
	        ano2.set("MAI", Double.parseDouble(listaLuz.get(1).getMaio().replaceAll( "," , "." )));
	 
	        model.addSeries(ano1);
	        model.addSeries(ano2);
	 
	        return model;
	    }
	
	
	  public BarChartModel getBarModel() {
	        return barModel;
	    }
	 
	    public HorizontalBarChartModel getHorizontalBarModel() {
	        return horizontalBarModel;
	    }
	
	private void createBarModels() {
        createBarModel();
       
    }
 
    private void createBarModel() {
        barModel = initBarModel();
 
        barModel.setTitle("Bar Chart");
        barModel.setLegendPosition("ne");
 
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Mês");
 
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Concumo");
        yAxis.setMin(0);
        yAxis.setMax(100000000);
    }
    
    
  /*  private void createAnimatedModels() {
        
    	 
        animatedModel2 = initBarModel();
        animatedModel2.setTitle("Bar Charts");
        animatedModel2.setAnimate(true);
        animatedModel2.setLegendPosition("ne");
      
        Axis yAxis = animatedModel2.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(10000000);
    }
 */
    
   
		

}
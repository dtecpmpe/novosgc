package br.gov.pe.pm.esgc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.gov.pe.pm.esgc.model.Conta;
import br.gov.pe.pm.esgc.model.Grupo;
import br.gov.pe.pm.esgc.model.Imovel;
import br.gov.pe.pm.esgc.model.TipoConta;
import br.gov.pe.pm.esgc.rn.ContaRn;
import br.gov.pe.pm.esgc.rn.GrupoRn;
import br.gov.pe.pm.esgc.rn.ImovelRn;
import br.gov.pe.pm.esgc.rn.TipoContaRn;
import br.gov.pe.pm.esgc.util.method.MetodosUtil;

@Named("cadastroContasBean")
@ViewScoped
public class CadastroContasBean implements Serializable {

	private static final long serialVersionUID = -8339536807145293341L;
	private List<TipoConta> listaTipoContas;
	private List<Imovel> listaImovel;
	private TipoConta tipoConta = new TipoConta();
	private List<Conta> listaContas;
	private List<SelectItem> listaConta;
	private Conta conta = new Conta();
	private Conta contaAnterior = new Conta();
	private Imovel imovel = new Imovel();
	private Integer imov;
	
	// Getters and Setters
	public List<TipoConta> getListaTipoContas() {
		return listaTipoContas;
	}

	public void setListaTipoContas(List<TipoConta> listaTipoContas) {
		this.listaTipoContas = listaTipoContas;
	}

	public void setListaImovel(List<Imovel> listaImovel) {
		this.listaImovel = listaImovel;
	}

	public List<Imovel> getListaImovel() {
		return listaImovel;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public List<Conta> getListaContas() {
		return listaContas;
	}

	public void setListaContas(List<Conta> listaContas) {
		this.listaContas = listaContas;
	}

	public List<SelectItem> getListaConta() {
		return listaConta;
	}

	public void setListaConta(List<SelectItem> listaConta) {
		this.listaConta = listaConta;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.contaAnterior = conta.clone();
		this.conta = conta;
	}

	public Conta getContaAnterior() {
		return contaAnterior;
	}

	public void setContaAnterior(Conta contaAnterior) {
		this.contaAnterior = contaAnterior;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	public Integer getImov() {
		return imov;
	}

	public void setImov(Integer imov) {
		this.imov = imov;
	}

// Methods

	@PostConstruct
	public void init() {
		this.imov = Integer
				.parseInt((String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("imov"));
		imovel = new ImovelRn().carregar(this.imov);
		this.conta.setImovel(imovel);
		listar();
	}

	public void listar() {
		this.listaContas = new ContaRn().listarPorImovel(imovel);
	}

	public CadastroContasBean() {
		imovel = new Imovel();
		conta.setImovel(imovel);
	}

	public String excluirConta() {
		new ContaRn().excluir(this.conta);
		contaAnterior = new Conta();
		listar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}

	public String removeAcentos(String s) {
		return new MetodosUtil().removeAcentos(s);
	}

	public List<SelectItem> getListarTipoContas() {
		List<SelectItem> listaTipoConta = new ArrayList<SelectItem>();
		List<TipoConta> tipoContas = new TipoContaRn().listarAtivos();
		listaTipoConta.add(new SelectItem(null, "Selecione..."));
		for (TipoConta tc : tipoContas) {
			listaTipoConta.add(new SelectItem(tc.getIdTipoConta(), tc.getDescricao()));

		}
		return listaTipoConta;
	}

	public List<SelectItem> getListarGrupos() {
		List<SelectItem> listaGrupo = new ArrayList<SelectItem>();
		listaGrupo.add(new SelectItem(null, "Selecione..."));
			if (this.conta.getTipoConta().getIdTipoConta() != (null) && !(this.conta.getTipoConta().getIdTipoConta().equals(null))) {
				List<Grupo> grupo = new GrupoRn().listarAtivoConta(this.conta.getTipoConta());
				for (Grupo g : grupo) {
					listaGrupo.add(new SelectItem(g.getIdGrupo(), g.getGrupo()));
				}
			return listaGrupo;
		}
		return null;
	}

	public void novo() {
		this.conta = new Conta();
		contaAnterior = new Conta();
		this.conta.setStatus(true);
		this.conta.setImovel(imovel);
		this.conta.getTipoConta().setIdTipoConta(getTipoConta().getIdTipoConta());		
	}

	public void salvar() {
		this.conta = new ContaRn().salvar(this.contaAnterior, this.conta);
		listar(); // mensagem de sucesso FacesMessage message = new
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext.getCurrentInstance().update(Arrays.asList("msgs", "tb_conta"));
	}	

	public String editar(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", id);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("call", "1");
		return "cadastro_contas?faces-redirect=true";
	}

}

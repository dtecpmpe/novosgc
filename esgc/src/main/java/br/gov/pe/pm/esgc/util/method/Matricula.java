package br.gov.pe.pm.esgc.util.method;

public class Matricula {

	private static String matricula;

	/*
	 * Irá converter Matricula formatado para sem pontos e traço. Ex.: 104.192-4
	 * torna-se 1041924.
	 */
	public static String retirarMascara(String value) {
		matricula = value;
		if (value != null && !value.equals("")) {
			matricula = value.replaceAll("\\.", "").replaceAll("\\-", "");
			matricula = matricula.replaceFirst("^0+(?!$)", "");
		}
		return matricula;
	}

	/*
	 * Irá converter Matricula formatado sem traço pra com traço. Ex.: 1041924
	 * torna-se 104192-4. com zero para sem zero. Ex.: 0287504 torna-se 28750-4.
	 */
	public static String colocarMascara(String value) {
		matricula = value;
		if (matricula.length() < 7) {
			matricula = "0" + matricula;
		}
		if (matricula != null && matricula.length() == 7) {
			matricula = matricula.substring(0, 6) + "-" + matricula.substring(6, 7);
			matricula = matricula.replaceFirst("^0+(?!$)", "");
		}
		return matricula;
	}

	/*
	 * Irá calcula o digito verivicador da matricula retornando o digito
	 * verivicador
	 */
	public static String calcDigVerif(String num) {
		if (num.length() == 5) {
			num = "0" + num;
		} else if (num.length() == 4) {
			num = "00" + num;
		}
		Integer primDig;
		int soma = 0, peso = 7;
		for (int i = 0; i < num.length(); i++)
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;
		if (soma % 11 == 0 | soma % 11 == 1)
			primDig = new Integer(0);
		else
			primDig = new Integer(11 - (soma % 11));
		return primDig.toString();
	}

	/*
	 * Irá validar ma matricula retornando true ou false
	 */
	public static boolean validarMatricula(String matricula) {
		if (matricula == null || (matricula.length() != 7 && matricula.length() != 6) || isMatriculaPadrao(matricula))
			return false;
		if (matricula.length() == 6) {
			return calcDigVerif(matricula.substring(0, 5)).equals(matricula.substring(5, 6));
		} else if (matricula.length() == 7)
			return calcDigVerif(matricula.substring(0, 6)).equals(matricula.substring(6, 7));
		return false;
	}

	private static boolean isMatriculaPadrao(String matricula) {
		if (matricula.equals("1111111") || matricula.equals("2222222") || matricula.equals("3333333")
				|| matricula.equals("4444444") || matricula.equals("5555555") || matricula.equals("6666666")
				|| matricula.equals("7777777") || matricula.equals("8888888") || matricula.equals("9999999")
				|| matricula.equals("111111") || matricula.equals("222222") || matricula.equals("333333")
				|| matricula.equals("4444444") || matricula.equals("555555") || matricula.equals("666666")
				|| matricula.equals("777777") || matricula.equals("888888") || matricula.equals("999999")) {

			return true;
		}

		return false;
	}
	
	public static String gerarMatricula(Integer matricula) {
		try{
		String mat = matricula.toString() + calcDigVerif(matricula.toString());
		if(validarMatricula(mat))
			return mat;
		else
			return null;	
		}catch (Error e){
			System.err.println(e.getMessage());
			return null;
		}
	}

/*	public static void main(String[] args) {

		String matricula = "104192-4";
		String matricula0 = "028.750-4";
		String matricula1 = "28.750-4";
		String matricula2 = "1041924";
		String matricula3 = "287504";
		String matricula4 = "0287504";

		System.out.println(retirarMascara(matricula));
		System.out.println(retirarMascara(matricula0));
		System.out.println(retirarMascara(matricula1));
		
		System.out.println(colocarMascara(matricula2));
		System.out.println(colocarMascara(matricula3));
		System.out.println(colocarMascara(matricula4));

		// Calcula o digito verificador
		System.out.println(calcDigVerif("029450"));
		System.out.println(calcDigVerif("29450"));
		System.out.println(calcDigVerif("104192"));
		System.out.println(calcDigVerif("108625"));

		// Valida a matricula
		System.out.println(validarMatricula("294500"));
		System.out.println(validarMatricula("1041924"));
		System.out.println(validarMatricula("1088343"));
		System.out.println(validarMatricula("0105619"));
		System.out.println(validarMatricula("7777777"));
		
		System.out.println(gerarMatricula(108834));

	}
*/
}
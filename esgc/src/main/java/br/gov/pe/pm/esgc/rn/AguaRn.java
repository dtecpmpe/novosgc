package br.gov.pe.pm.esgc.rn;

import java.util.List;

import br.gov.pe.pm.esgc.hibernateDAO.AguaHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.AguaDAO;
import br.gov.pe.pm.esgc.model.Agua;
import br.gov.pe.pm.esgc.util.db.DAOFactory;

public class AguaRn {
	
	private AguaDAO aguaDAO = new AguaHibernateDAO();

	public AguaRn() {
		this.aguaDAO = (AguaDAO) DAOFactory.criarDAO(this.aguaDAO);
	}
	
	public List<Agua> listar() {
		return this.aguaDAO.listar();
	}
	
	public List<Agua> listarAnual() {
		return this.aguaDAO.listarAnual();
	}
	
}

package br.gov.pe.pm.esgc.rn;

import java.util.List;

import br.gov.pe.pm.esgc.hibernateDAO.LuzHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.LuzDAO;
import br.gov.pe.pm.esgc.model.Luz;
import br.gov.pe.pm.esgc.model.LuzAnual;
import br.gov.pe.pm.esgc.util.db.DAOFactory;

public class LuzRn {
	
	private LuzDAO luzDAO = new LuzHibernateDAO();

	public LuzRn() {
		this.luzDAO = (LuzDAO) DAOFactory.criarDAO(this.luzDAO);
	}
	
	public List<Luz> listar() {
		return this.luzDAO.listar();
	}
	
	public List<Luz> listarAnual() {
		return this.luzDAO.listarAnual();
	}
	
}

package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.entity.Pessoa;

 

public interface PessoaDAO extends Dao {  

	public Pessoa salvar(Pessoa pessoa); 
	public void excluir(Pessoa pessoa); 
	public Pessoa carregar(Integer idPessoa);
	public Pessoa carregarPorCpf(String cpf);
	public List<Pessoa> listar();
	public List<Pessoa> listarPorNome(String nomeCompleto);
	public List<Pessoa> listarPorCPF(String cpf);

}
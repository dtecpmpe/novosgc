package br.gov.pe.pm.esgc.bean;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.gov.pe.pm.esgc.entity.entityview.OmeView;
import br.gov.pe.pm.esgc.model.Competencia;
import br.gov.pe.pm.esgc.model.Conta;
import br.gov.pe.pm.esgc.model.Imovel;
import br.gov.pe.pm.esgc.model.TipoConta;
import br.gov.pe.pm.esgc.model.TipoFatura;
import br.gov.pe.pm.esgc.rn.CompetenciaRn;
import br.gov.pe.pm.esgc.rn.ContaRn;
import br.gov.pe.pm.esgc.rn.ImovelRn;
import br.gov.pe.pm.esgc.rn.OmeRn;
import br.gov.pe.pm.esgc.rn.TipoContaRn;
import br.gov.pe.pm.esgc.rn.TipoFaturaRn;
import br.gov.pe.pm.esgc.util.method.MetodosUtil;

@Named("lancamentoFaturaBean")
@ViewScoped
public class LancamentoFaturaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public LancamentoFaturaBean() {
	};

	private OmeView omeView = new OmeView();
	private List<SelectItem> listaFatura;
	private List<Conta> listaConta;
	private List<Competencia> listaCompetencia;
	private Competencia competencia = new Competencia();
	private Competencia competenciaAnterior = new Competencia();
	private Conta conta = new Conta();
	private Conta contaAnterior = new Conta();
	private Imovel imovel = new Imovel();
	private List<Conta> ListaContas = new ArrayList<Conta>();
	private List<Imovel> listaImovel;
	private String ncm;
	private Double consumoP;
	private Double consumoT;
	private String teste;
	private Integer tipoConta;

	// Getters and Setters

	public Integer getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(Integer tipoConta) {
		this.tipoConta = tipoConta;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public List<Conta> getListaContas() {
		return ListaContas;
	}

	public void setListaContas(List<Conta> listaContas) {
		ListaContas = listaContas;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	public OmeView getOmeView() {
		return omeView;
	}

	public void setOmeView(OmeView omeView) {
		this.omeView = omeView;
	}

	public List<SelectItem> getListaFatura() {
		return listaFatura;
	}

	public void setListaFatura(List<SelectItem> listaFatura) {
		this.listaFatura = listaFatura;
	}

	public List<Conta> getListaConta() {
		return listaConta;
	}

	public void setListaConta(List<Conta> listaConta) {
		this.listaConta = listaConta;
	}

	public List<Competencia> getListaCompetencia() {
		return listaCompetencia;
	}

	public void setListaCompetencia(List<Competencia> listaCompetencia) {
		this.listaCompetencia = listaCompetencia;
	}

	public Competencia getCompetencia() {
		return competencia;
	}

	public void setCompetencia(Competencia competencia) {
		this.competenciaAnterior = competenciaAnterior.clone();
		this.competencia = competencia;
	}

	public Competencia getCompetenciaAnterior() {
		return competenciaAnterior;
	}

	public void setCompetenciaAnterior(Competencia competenciaAnterior) {
		this.competenciaAnterior = competenciaAnterior;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.contaAnterior = conta.clone();
		this.conta = conta;
	}

	public Conta getContaAnterior() {
		return contaAnterior;
	}

	public void setContaAnterior(Conta contaAnterior) {
		this.contaAnterior = contaAnterior;
	}

	public void listarConta() {
		this.setListaConta(new ContaRn().listar());
	}

	public void listarCompetencia() {
		this.setListaCompetencia(new CompetenciaRn().listar());
	}

	public void listarCompetenciaPorTipoConta() {
		this.setListaCompetencia(new CompetenciaRn().listar(this.competencia.getTipoConta()));
	}

	private Date comp;

	public Date getComp() {
		return comp;
	}

	public void setComp(Date comp) {
		this.comp = comp;
	}

	public Double getConsumoP() {
		return consumoP;
	}

	public void setConsumoP(Double consumoP) {

		this.consumoP = consumoP;
	}

	public Double getConsumoT() {
		return consumoT;
	}

	public void setConsumoT(Double consumoT) {
		this.consumoT = consumoT;
	}

	public String getTeste() {
		return teste;
	}

	public void setTeste(String teste) {
		this.teste = teste;
	}

	public List<Imovel> getListaImovel() {
		return listaImovel;
	}

	public void setListaImovel(List<Imovel> listaImovel) {
		this.listaImovel = listaImovel;
	}

	// Métodos
	public void listarCompetenciaPorCompetencia() {
		this.competencia.setTipoConta(new TipoContaRn().carregar(tipoConta));
		this.competencia.setCompetencia(getComp());
		this.setListaCompetencia(
				new CompetenciaRn().listar(this.competencia.getTipoConta(), this.competencia.getCompetencia()));
	}

	public void listarCompetenciaPorConta() {
		this.setListaCompetencia(new CompetenciaRn().listar(this.competencia.getTipoConta(),
				this.competencia.getCompetencia(), this.conta));
	}

	public List<SelectItem> getListarTipoFatura() {
		if (this.listaFatura == null) {
			this.listaFatura = new ArrayList<SelectItem>();
			List<TipoFatura> tf = new TipoFaturaRn().listar();
			this.listaFatura.add(new SelectItem(null, "Selecione..."));
			for (TipoFatura f : tf) {
				this.listaFatura.add(new SelectItem(f.getIdTipoFatura(), f.getDescricao()));
			}

		}
		return listaFatura;
	}

	public List<SelectItem> getListarGrupoContas() {

		List<SelectItem> listaTipoConta = new ArrayList<SelectItem>();
		List<TipoConta> tipoContas = new TipoContaRn().listarAtivos();
		listaTipoConta.add(new SelectItem(null, "Selecione..."));
		for (TipoConta tc : tipoContas) {
			listaTipoConta.add(new SelectItem(tc.getIdTipoConta(), tc.getDescricao()));

		}
		return listaTipoConta;
	}

	public void adcionar() {
		this.conta = new Conta();
		contaAnterior = new Conta();
		this.conta.setStatus(true);
		this.competencia = new Competencia();
		competenciaAnterior = new Competencia();
	}

	public void editar(Competencia competencia) {
		setCompetencia(competencia);
		setTipoConta(this.competencia.getTipoConta().getIdTipoConta());
		setComp(this.competencia.getCompetencia());
		setNcm(this.competencia.getConta().getnContratoMatricula());
		setConta(this.competencia.getConta());
		carregarDados();
		setOmeView(new OmeRn().carregar(this.competencia.getImovel().getIdOme()));
		listarCompetencia();
		setConta(this.competencia.getConta());
		setConsumoT(this.competencia.getConsumo());

	}


	public String excluir() {
		new ContaRn().excluir(this.conta);
		contaAnterior = new Conta();
		new CompetenciaRn().excluir(this.competencia);
		competenciaAnterior = new Competencia();
		listarCompetencia();
		setCompetencia(new Competencia());
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}

	public void salvar() {
		
		this.competencia.setConsumo(consumoT);
		this.competencia.setCompetencia(comp); 		
		this.competencia.setConta(new ContaRn().carregar(this.conta.getIdConta()));		
		this.competencia.getImovel().setIdImovel(this.conta.getImovel().getIdImovel());
		this.competencia.setTipoConta(new TipoContaRn().carregar(this.tipoConta));				
		this.competencia = new CompetenciaRn().salvar(this.competenciaAnterior, this.competencia);
		listarCompetenciaPorCompetencia();
		this.consumoT = null;
		this.consumoP = null;
		this.omeView = null;
		this.conta = new Conta();
		this.imovel = new Imovel();
		this.ncm = null;
		this.comp = null;
		this.competencia = new Competencia();

		// mensagem de sucesso
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext.getCurrentInstance().update(Arrays.asList("frm:msgs"));
	}

	public List<String> completarPorMatricula(String numero) {
		List<String> results = new ArrayList<String>();
		List<Conta> dado = new ContaRn().listarPorMatricula(numero);
		for (Conta d : dado) {			
			if (d.getTipoConta().getIdTipoConta().equals(tipoConta)) {
				String num = d.getnContratoMatricula();
				results.add(num);
			}
		}
		return results;

	}

	public void novoValor() {
		this.consumoP = null;
		this.consumoT = null;
	}

	public void carregarDados() {		
		this.conta = new ContaRn().carregarNCM(this.ncm);		
		omeView = new OmeRn().carregar(this.conta.getImovel().getIdOme());
		listarPorOme();
		listarCompetenciaPorConta();
	}

	public void listarPorOme() {
		this.setListaImovel(new ImovelRn().listarPorOme(omeView.getIdOrganizacao()));
	}

	public String removeAcentos(String s) {
		return new MetodosUtil().removeAcentos(s);
	}

	public String getNcm() {
		return ncm;
	}

	public void setNcm(String ncm) {
		this.ncm = ncm;
	}

	public void add() {
		if (this.consumoT == null) {
			this.consumoT = 0.0;
		}
		this.consumoT = this.consumoT + this.consumoP;
		this.consumoP = null;

	}

	public void limpar2() {
		this.consumoT = null;
		this.consumoP = null;
		this.omeView = null;
		this.conta = new Conta();
		this.imovel = new Imovel();
		this.ncm = null;
		this.setCompetencia(new Competencia());
		listarCompetenciaPorCompetencia();
		
	}

	public void limpar() {
		this.consumoT = null;
		this.consumoP = null;
		this.omeView = null;
		this.conta = new Conta();
		this.imovel = new Imovel();
		this.ncm = null;
		this.comp = null;
		this.competencia.setTipoFatura(new TipoFatura());
		this.competencia.setVencimento(null);
		this.competencia.setCompetencia(null);
		this.competencia.setTipoConta(new TipoContaRn().carregar(this.tipoConta));
		listarCompetenciaPorTipoConta();

	}

	public void limparTudo() {
		this.consumoT = null;
		this.consumoP = null;
		this.omeView = null;
		this.conta = new Conta();
		this.imovel = new Imovel();
		this.ncm = null;
		this.comp = null;
		this.omeView = null;
		this.conta = null;
		this.competencia.setTipoFatura(new TipoFatura());
		this.competencia.setVencimento(null);
		this.competencia.setCompetencia(null);
		this.competencia.setTipoConta(new TipoContaRn().carregar(this.tipoConta));
		listarCompetenciaPorTipoConta();

	}

}

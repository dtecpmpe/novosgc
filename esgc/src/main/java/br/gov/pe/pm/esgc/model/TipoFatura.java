package br.gov.pe.pm.esgc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipo_fatura", schema = "conta")
public class TipoFatura implements Serializable, Cloneable {
	

	private static final long serialVersionUID = 4340406981037699095L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id_tipo_fatura", nullable = false )
	private Integer idTipoFatura;
	
	//tipo de conta
	@Column
	private String descricao;

	@Column
	private Boolean status;
	
	@Column(name ="id_tipo_conta")
	private Integer idTipoConta;

	public Integer getIdTipoFatura() {
		return idTipoFatura;
	}

	public void setIdTipoFatura(Integer idTipoFatura) {
		this.idTipoFatura = idTipoFatura;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	
	public Integer getIdTipoConta() {
		return idTipoConta;
	}

	public void setIdTipoConta(Integer idTipoConta) {
		this.idTipoConta = idTipoConta;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((idTipoConta == null) ? 0 : idTipoConta.hashCode());
		result = prime * result + ((idTipoFatura == null) ? 0 : idTipoFatura.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoFatura other = (TipoFatura) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (idTipoConta == null) {
			if (other.idTipoConta != null)
				return false;
		} else if (!idTipoConta.equals(other.idTipoConta))
			return false;
		if (idTipoFatura == null) {
			if (other.idTipoFatura != null)
				return false;
		} else if (!idTipoFatura.equals(other.idTipoFatura))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public TipoFatura clone() {
		try {
			return (TipoFatura)super.clone();
		} catch (CloneNotSupportedException e) {
			//System.out.println("O objeto CIDADE não pode ser clonado." + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public String toString() {
		return "idTipoFatura= " + idTipoFatura + ", descricao= " + descricao + ", status= " + status + ", idTipoConta= " + idTipoConta + ".";
	}
	

}

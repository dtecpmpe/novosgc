package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.entity.Log;


public interface LogDAO extends Dao {

	public Log salvar(Log log);
	public List<Log> listar();
	
}

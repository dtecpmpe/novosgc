package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.Imovel;


public interface ImovelDAO extends Dao {
	
	public List<Imovel> listar();		
	public List<Imovel> listarPorOme(Integer ome);	
	public Imovel salvar(Imovel imovelNovo);	
	public void excluir(Imovel imovel);	
	public List<Imovel> listarAtivo();	
	public Imovel carregar(Integer id);
	
}

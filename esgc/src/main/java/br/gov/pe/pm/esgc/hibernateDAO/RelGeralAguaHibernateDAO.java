package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;

import br.gov.pe.pm.esgc.interfaceDAO.RelGeralAguaDAO;
import br.gov.pe.pm.esgc.model.RelGeralAgua;


public class RelGeralAguaHibernateDAO implements RelGeralAguaDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RelGeralAgua> listar() {	
		return (List<RelGeralAgua>)session.createCriteria(RelGeralAgua.class, "relGeralAgua").list();
	}

	
}
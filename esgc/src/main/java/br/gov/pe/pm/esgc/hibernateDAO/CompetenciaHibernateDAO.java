package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.esgc.interfaceDAO.CompetenciaDAO;
import br.gov.pe.pm.esgc.model.Competencia;
import br.gov.pe.pm.esgc.model.Conta;
import br.gov.pe.pm.esgc.model.TipoConta;

public class CompetenciaHibernateDAO implements CompetenciaDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Competencia> listar() {	
		return (List<Competencia>)session.createCriteria(Competencia.class, "competencia").list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Competencia> listar(TipoConta tipoConta) {
		return (List<Competencia>)session.createCriteria(Competencia.class, "competencia").add(Restrictions.eq("tipoConta", tipoConta)).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Competencia> listar(TipoConta tipoConta, Date competencia) {
		return (List<Competencia>)session.createCriteria(Competencia.class, "competencia").add(Restrictions.eq("tipoConta", tipoConta)).add(Restrictions.eq("competencia", competencia)).list();
	}

	@SuppressWarnings("unchecked")
	public List<Competencia> listar(TipoConta tipoConta, Date competencia, Conta conta) {
		return (List<Competencia>)session.createCriteria(Competencia.class, "competencia").add(Restrictions.eq("tipoConta", tipoConta)).add(Restrictions.eq("competencia", competencia)).add(Restrictions.eq("conta", conta)).list();
	}	

	@Override
	public Competencia salvar(Competencia competencia) {		
		Competencia retorno =(Competencia)this.session.merge(competencia);		
		this.session.flush();
		return retorno;		
	}

	@Override
	public void excluir(Competencia competencia) {
		this.session.delete(competencia);		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Competencia> listarAtivo() {
		return (List<Competencia>)session.createCriteria(Competencia.class, "competencia").add(Restrictions.eq("ativo", true)).list();
	}
	
	public Competencia carregar(Integer id) {
		return (Competencia)session.createCriteria(Competencia.class, "competencia").add(Restrictions.eq("idCompetencia", id)).uniqueResult();
	}
}
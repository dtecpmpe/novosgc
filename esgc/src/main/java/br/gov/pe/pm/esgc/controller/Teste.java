package br.gov.pe.pm.esgc.controller;

import javax.annotation.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@ManagedBean
@SessionScoped
public class Teste {

    double valor1;
    double valor2;
    double total;

    public Teste() {
        super();
    }

    public Teste(double valor1, double valor2, double total) {
        super();
        this.valor1 = valor1;
        this.valor2 = valor2;
        this.total = total;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

	/*
	 * public double somar() {
	 * 
	 * Soma soma = new Soma(); soma.setValor1(soma.getValor1());
	 * soma.setValor2(soma.getValor2()); double total = this.valor1 + this.valor2;
	 * 
	 * return total;
	 * 
	 * 
	 * }
	 */

    public double getValor1() {
        return valor1;
    }

    public void setValor1(double valor1) {
        this.valor1 = valor1;
    }

    public double getValor2() {
        return valor2;
    }

    public void setValor2(double valor2) {
        this.valor2 = valor2;
    }

}

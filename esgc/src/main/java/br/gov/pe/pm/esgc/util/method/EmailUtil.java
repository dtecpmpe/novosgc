
package br.gov.pe.pm.esgc.util.method;

import org.apache.commons.mail.HtmlEmail;

public class EmailUtil {

	public EmailUtil() {

	}

	public void emailEsqueceu(String emailDestino, String login, String token, String urlBase) {
		try {

			// configurando email
			HtmlEmail email = new HtmlEmail();
			email.setHostName("smtp.expresso.pe.gov.br");
			email.setAuthentication("sistemasdtec", "");
			email.addTo(emailDestino);
			email.setFrom("sistemasdtec@pm.pe.gov.br");
			email.setSubject("Acesso SGPM");

			email.setHtmlMsg("<html> <body> <h3>SGPM - Sistema de Gest&atildeo Policial Militar </h3>"
					+ "<h4>Ol&aacute,</h4>"
					+ "<p>este e-mail foi enviado a partir do formul&aacuterio \"Esqueceu o login ou a senha?\" do <br>"
					+ "SGPM - Sistema de Gest&atildeo Policial Militar.<br>"
					+ "A seguir informamos os seus dados de acesso ao sistema: </p><br>" + "<p>&nbsp;&nbsp;Login: <b>\""
					+ login + "\"</b> <br><br>"
					+ "&nbsp;&nbsp;Para redefinir a sua senha de acesso, clique no link abaixo:<br><br>"
					+ "&nbsp;&nbsp;<a href=" + urlBase + ">" + urlBase + "</a></p>"
					+ "<p> <br><br> Atenciosamente,<br> DTEC - Diretoria de Tecnologia </p>"
					+ "<h5 style=\"color:red\">* Caso voc&ecirc n&atildeo tenha solicitado a redefini&ccedil&atildeo de senha, desconsidere este e-mail. <br>"
					+ " ** Este link de redefini&ccedil&atildeo de senha ser&aacute desativado 24 horas ap&oacutes a sua solicita&ccedil&atildeo. </h5> </html>");

			email.send();

		} catch (Exception e) {

			// System.out.println("Erro ao enviar o e-mail: " + e.getMessage());

		}

	}

	public void emailCadastro(String emailDestino, String login, String senha) {
		try {

			// configurando email 
			HtmlEmail email = new HtmlEmail();
			email.setHostName("smtp.expresso.pe.gov.br");
			email.setAuthentication("sistemasdtec", "");
			email.addTo(emailDestino);
			email.setFrom("sistemasdtec@pm.pe.gov.br");
			email.setSubject("Cadastro SGPM");

			email.setHtmlMsg("<html> <body> <h3>SGPM - Sistema de Gest&atildeo Policial Militar </h3>"
					+ "<h4>Ol&aacute,</h4>"
					+ "<p>este e-mail foi enviado a partir do formul&aacuterio de cadastro do <br>"
					+ "SGPM - Sistema de Gest&atildeo Policial Militar. <br>"
					+ "A seguir informamos os seus dados de acesso ao sistema: </p>" + "<p>&nbsp;&nbsp;Login: <b>\""
					+ login + "\"</b> <br> &nbsp;&nbsp;Senha: <b>\"" + senha + "\"</b> </p>"
					+ "<p> Caso voc&ecirc queira alterar sua senha, isso pode ser feito a qualquer momento <br>"
					+ "atrav&eacutes do menu principal em: <b>Configura&ccedil&otildees>Seguran&ccedila>Alterar Senha</b><br><br> Atenciosamente,<br> DTEC - Diretoria de Tecnologia </p> <br><br> </html>");

			email.send();

		} catch (Exception e) {

			// System.out.println("Erro ao enviar o e-mail: " + e.getMessage());

		}

	}

	public void emailRedefinicao(String emailDestino, String login, String senha) {
		try {

			// configurando email 
			HtmlEmail email = new HtmlEmail();
			email.setHostName("smtp.expresso.pe.gov.br");
			email.setAuthentication("sistemasdtec", "");
			email.addTo(emailDestino);
			email.setFrom("sistemasdtec@pm.pe.gov.br");
			email.setSubject("Acesso SGPM");

			email.setHtmlMsg("<html> <body> <h3>SGPM - Sistema de Gest&atildeo Policial Militar </h3>"
					+ "<h4>Ol&aacute,</h4>"
					+ "<p>este e-mail foi enviado a partir do formul&aacuterio \"Esqueceu o login ou a senha?\" do <br>"
					+ "SGPM - Sistema de Gest&atildeo Policial Militar.<br>"
					+ "A seguir informamos os seus dados de acesso ao sistema: </p><br>" + "<p>&nbsp;&nbsp;Login: <b>\""
					+ login + "\"</b> <br><br>" + "&nbsp;&nbsp;Nova Senha: \"" + senha + "\"</b> <br><br>"
					+ "<p> Caso voc&ecirc queira alterar sua senha, isso pode ser feito a qualquer momento <br>"
					+ "atrav&eacutes do menu principal em: <b>Configura&ccedil&otildees>Seguran&ccedila>Alterar Senha</b><br><br> Atenciosamente,<br> DTEC - Diretoria de Tecnologia </p> <br><br> </html>");

			email.send();

		} catch (Exception e) {

			// System.out.println("Erro ao enviar o e-mail: " + e.getMessage());

		}

	}

}

package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;

import br.gov.pe.pm.esgc.interfaceDAO.RelGeralLuzDAO;
import br.gov.pe.pm.esgc.model.RelGeralLuz;

public class RelGeralLuzHibernateDAO implements RelGeralLuzDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RelGeralLuz> listar() {	
		return (List<RelGeralLuz>)session.createCriteria(RelGeralLuz.class, "relGeralLuz").list();
	}

	
}
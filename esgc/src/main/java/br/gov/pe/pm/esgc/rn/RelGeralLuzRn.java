package br.gov.pe.pm.esgc.rn;

import java.util.List;

import br.gov.pe.pm.esgc.hibernateDAO.RelGeralLuzHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.RelGeralLuzDAO;
import br.gov.pe.pm.esgc.model.RelGeralLuz;
import br.gov.pe.pm.esgc.util.db.DAOFactory;

public class RelGeralLuzRn {
	
	private RelGeralLuzDAO relGeralLuzDAO = new RelGeralLuzHibernateDAO();

	public RelGeralLuzRn() {
		this.relGeralLuzDAO = (RelGeralLuzDAO) DAOFactory.criarDAO(this.relGeralLuzDAO);
	}
	
	public List<RelGeralLuz> listar() {
		return this.relGeralLuzDAO.listar();
	}
	
}

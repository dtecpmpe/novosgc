package br.gov.pe.pm.esgc.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.gov.pe.pm.esgc.model.Competencia;
import br.gov.pe.pm.esgc.util.exception.UtilException;
import br.gov.pe.pm.esgc.util.method.RelatorioUtil;

@ManagedBean(name = "relatorioBean")
@ViewScoped
public class RelatorioBean implements Serializable {

	private static final long serialVersionUID = -659256471177401598L;
	private Competencia competencia = new Competencia();

	public void pesquisar() {
		getArquivoRetorno("competencia_cel");
	}
	

	// MÉTODO DO RELATÓRIO
	public void getArquivoRetorno(String nome) {
		FacesContext context = FacesContext.getCurrentInstance();
		String nomeRelatorioJasper = nome;
		RelatorioUtil relatorioUtil = new RelatorioUtil();
		HashMap<String, Object> parametrosRelatorio = new HashMap<String, Object>();

		String caminho = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/resources/img/brasao_pmpe.png");

		parametrosRelatorio.put("logo", caminho);

		try {
			relatorioUtil.geraRelatorio(parametrosRelatorio, nome);

		} catch (UtilException e) {
			context.addMessage(null, new FacesMessage(e.getMessage()));
		}

	}

	public Competencia getCompetencia() {
		return competencia;
	}

	public void setCompetencia(Competencia competencia) {
		this.competencia = competencia;
	}

}

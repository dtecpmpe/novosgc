package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.esgc.interfaceDAO.OrganizacaoEnderecoDAO;
import br.gov.pe.pm.esgc.model.Imovel;
import br.gov.pe.pm.esgc.model.OrganizacaoEndereco;



public class OrganizacaoEnderecoHibernateDAO implements OrganizacaoEnderecoDAO {
	
	private Session session;

	
	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public OrganizacaoEndereco salvar(OrganizacaoEndereco omeEndereco) {
		return (OrganizacaoEndereco)this.session.merge(omeEndereco);	
	}
	
	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public List<Imovel> listarPorOme(Integer ome) { return
	 * (List<Imovel>)session.createCriteria(Imovel.class,
	 * "imovel").add(Restrictions.eq("idOme", ome)).list(); }
	 */
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrganizacaoEndereco> listarPorOme(Integer organizacao){
		return (List<OrganizacaoEndereco>)session.createCriteria(OrganizacaoEndereco.class, "organizacao_endereco").add(Restrictions.eq("organizacao",organizacao)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrganizacaoEndereco> listar() {	
		return (List<OrganizacaoEndereco>)session.createCriteria(OrganizacaoEndereco.class, "organizacao_endereco").list();
	}
	

	@Override
	public void excluir(OrganizacaoEndereco organizacaoEndereco) {
		this.session.delete(organizacaoEndereco);		
	}

	@Override
	public OrganizacaoEndereco carregar(Integer id) {		
		return (OrganizacaoEndereco)session.createCriteria(OrganizacaoEndereco.class, "organizacao_endereco").add(Restrictions.eq("id_organizacao", id)).uniqueResult();
	}

	


}

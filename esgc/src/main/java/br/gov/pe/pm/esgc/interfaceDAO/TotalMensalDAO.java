package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.TotalMensal;

public interface TotalMensalDAO extends Dao {

	public List<TotalMensal> listar();
	
	public TotalMensal salvar(TotalMensal totalMensal);
	
	public void excluir(TotalMensal totalMensal);
	
	public List<TotalMensal> listarAtivo();
	
	public TotalMensal carregar(Integer id);
}


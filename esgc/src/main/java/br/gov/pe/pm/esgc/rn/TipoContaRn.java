package br.gov.pe.pm.esgc.rn;

import java.util.List;

import br.gov.pe.pm.esgc.hibernateDAO.TipoContaHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.TipoContaDAO;
import br.gov.pe.pm.esgc.model.TipoConta;
import br.gov.pe.pm.esgc.util.db.DAOFactory;




public class TipoContaRn {
	
	private TipoContaDAO tipoContaDAO = new TipoContaHibernateDAO();
	private LogRn logRn = new LogRn();
	public TipoContaRn() {
		this.tipoContaDAO = (TipoContaDAO) DAOFactory.criarDAO(this.tipoContaDAO);
	}
	
	public List<TipoConta> listar() {
		return this.tipoContaDAO.listar();
	}
	public void excluir(TipoConta objetoAntigo) {
		this.tipoContaDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}
	public TipoConta salvar(TipoConta objetoAntigo, TipoConta tipoContaNova) {
		TipoConta objetoNovo = this.tipoContaDAO.salvar( tipoContaNova);
		if (objetoNovo.getIdTipoConta() == null)
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
		else
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(),objetoNovo.toString());
		return objetoNovo;
	}
	
	public List<TipoConta> listarAtivos() {
		return this.tipoContaDAO.listarAtivos();		
	}
	
	public TipoConta carregar(Integer id) {
		// TODO Auto-generated method stub
		return (TipoConta)this.tipoContaDAO.carregar(id);
	}
}

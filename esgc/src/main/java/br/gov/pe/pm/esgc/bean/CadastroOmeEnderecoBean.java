package br.gov.pe.pm.esgc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.gov.pe.pm.esgc.entity.entityview.OmeView;
import br.gov.pe.pm.esgc.model.Imovel;
import br.gov.pe.pm.esgc.model.OrganizacaoEndereco;
import br.gov.pe.pm.esgc.rn.ImovelRn;

import br.gov.pe.pm.esgc.rn.OmeRn;
import br.gov.pe.pm.esgc.rn.OrganizacaoEnderecoRn;
import br.gov.pe.pm.esgc.util.method.MetodosUtil;


@Named("cadastroOmeEnderecoBean")
@ViewScoped
public class CadastroOmeEnderecoBean implements Serializable {
	
	private static final long serialVersionUID = 7072235400668163889L;
	
	private List<SelectItem> listaOrganizacao;
	private List<OrganizacaoEndereco> listaOmeEndereco;
	private List<OrganizacaoEndereco> listarPorOme;	
	private List<Imovel> listarImovel;
	private OmeView omeView = new OmeView();
	private OrganizacaoEndereco organizacaoEndereco = new OrganizacaoEndereco();
	private OrganizacaoEndereco organizacaoEnderecoAnterior = new OrganizacaoEndereco();
	
	//Getters and Setters
	public List<SelectItem> getListaOrganizacao() {
		return listaOrganizacao;
	}

	public void setListaOrganizacao(List<SelectItem> listaOrganizacao) {
		this.listaOrganizacao = listaOrganizacao;
	}

	public List<OrganizacaoEndereco> getListaOmeEndereco() {
		return listaOmeEndereco;
	}

	public void setListaOmeEndereco(List<OrganizacaoEndereco> listaOmeEndereco) {
		this.listaOmeEndereco = listaOmeEndereco;
	}

	public List<OrganizacaoEndereco> getListarPorOme() {
		return listarPorOme;
	}

	public void setListarPorOme(List<OrganizacaoEndereco> listarPorOme) {
		this.listarPorOme = listarPorOme;
	}

	public List<Imovel> getListarImovel() {
		return listarImovel;
	}

	public void setListarImovel(List<Imovel> listarImovel) {
		this.listarImovel = listarImovel;
	}

	public OmeView getOmeView() {
		return omeView;
	}

	public void setOmeView(OmeView omeView) {
		this.omeView = omeView;
	}

	public OrganizacaoEndereco getOrganizacaoEndereco() {
		return organizacaoEndereco;
	}

	public void setOrganizacaoEndereco(OrganizacaoEndereco organizacaoEndereco) {
		this.organizacaoEndereco = organizacaoEndereco;
	}

	public OrganizacaoEndereco getOrganizacaoEnderecoAnterior() {
		return organizacaoEnderecoAnterior;
	}

	public void setOrganizacaoEnderecoAnterior(OrganizacaoEndereco organizacaoEnderecoAnterior) {
		this.organizacaoEnderecoAnterior = organizacaoEnderecoAnterior;
	}
	
	
	
	
	
	//Métodos
	
	private void listar() {
		this.setListaOmeEndereco(new OrganizacaoEnderecoRn().listar());		
	}
	
	public void listarImovel() {
		this.setListarImovel(new ImovelRn().listar());
	}	

	public void listarPorOme() {
		this.setListarImovel(new ImovelRn().listarPorOme(omeView.getIdOrganizacao()));
	}
	
	public void carregarDados() {
		omeView = new OmeRn().carregar(omeView.getIdOrganizacao());
		listarPorOme();
	}
	
	public String removeAcentos(String s) {
		return new MetodosUtil().removeAcentos(s);
	}
	
	public List<SelectItem> getListarOmeEndereco() {
		if (this.listaOrganizacao == null) {
			this.listaOrganizacao = new ArrayList<SelectItem>();
			List<OmeView> pv = new OmeRn().listarOme();
			this.listaOrganizacao.add(new SelectItem(null, "Selecione..."));
			for (OmeView p : pv) {
				this.listaOrganizacao.add(new SelectItem(p.getIdOrganizacao(), p.getSigla()));
			}
		}
		return listaOrganizacao;
	}
	

	public void novo() {
		this.organizacaoEndereco = new OrganizacaoEndereco();
		organizacaoEnderecoAnterior = new OrganizacaoEndereco();		
			
	}
	
	public void salvar() {		
		this.organizacaoEndereco.setOrganizacao(omeView.getIdOrganizacao());		
		this.organizacaoEndereco.setNome(omeView.getNome());
		this.organizacaoEndereco.setEstado("PE");	
		this.organizacaoEndereco.setComplemento("BATALHÃO");
		this.organizacaoEndereco = (OrganizacaoEndereco) new OrganizacaoEnderecoRn().salvar(this.organizacaoEnderecoAnterior, this.organizacaoEndereco);		
		listar();
		// mensagem de sucesso
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		//RequestContext.getCurrentInstance().update(Arrays.asList(" msgs_dialog", "frm:tb_ome"));
	}
	
	public String excluir() {
		new OrganizacaoEnderecoRn().excluir(this.organizacaoEndereco);
		organizacaoEndereco = new OrganizacaoEndereco();
		listar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}
	public String editar(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", id);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("call", "1");
		return "ome?faces-redirect=true";				
	}

}

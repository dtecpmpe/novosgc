package br.gov.pe.pm.esgc.bean;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;

import br.gov.pe.pm.esgc.model.Luz;
import br.gov.pe.pm.esgc.rn.LuzRn;

@Named("luzBean")
@ViewScoped
public class LuzBean implements Serializable {

	private static final long serialVersionUID = -215057514692187770L;
	// private List<Luz> aguas;
	private int anoCorrente = new GregorianCalendar().get(java.util.Calendar.YEAR);
	private int anoAnterior = anoCorrente - 1;
	private int mes = new GregorianCalendar().get(java.util.Calendar.MONTH);
	private String mes1;
	private String mes2;
	private String mes3;

	public List<Luz> getLuzs() {
		return new LuzRn().listar();
	}
	
	public List<Luz> getLuzsAnual() {
		return new LuzRn().listarAnual();
	}

	/*
	 * public String getLastYearTotal() { double total = 0.0;
	 * 
	 * for(Luz agua : getAguas()) { total += agua.getLastYearAgua(); }
	 * 
	 * return new DecimalFormat("###,###.00").format(total); }
	 */

	/*
	 * public String getThisYearTotal() { double total = 0.0;
	 * 
	 * for(Luz agua : getAguas()) { total += agua.getThisYearAgua();
	 * 
	 * }
	 * 
	 * 
	 * return new DecimalFormat("###,###.00").format(total); }
	 */

	public int getAnoCorrente() {
		return anoCorrente;
	}

	public void setAnoCorrente(int anoCorrente) {
		this.anoCorrente = anoCorrente;
	}

	public int getAnoAnterior() {
		return anoAnterior;
	}

	public void setAnoAnterior(int anoAnterior) {
		this.anoAnterior = anoAnterior;
	}

	public String getDataAtual() {
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		Date dataAtual = new Date(System.currentTimeMillis());
		return sd.format(dataAtual);

	}

	public String getMes1() {
		return mes1;
	}

	public String getMes2() {
		return mes2;
	}

	public String getMes3() {
		return mes3;
	}

	public void carregarMes() {

		if (mes > 0) {
			switch (mes) {
			case 1:
				mes3 = "Jan";
				mes2 = "Dez";
				mes1 = "Nov";
				break;
			case 2:
				mes3 = "Fev";
				mes2 = "Jan";
				mes1 = "Dez";
				break;
			case 3:
				mes3 = "Mar";
				mes2 = "Fev";
				mes1 = "Jan";
				break;
			case 4:
				mes3 = "Abr";
				mes2 = "Mar";
				mes1 = "Fev";
				break;
			case 5:
				mes3 = "Mai";
				mes2 = "Abr";
				mes1 = "Mar";
				break;
			case 6:
				mes3 = "Jun";
				mes2 = "Mai";
				mes1 = "Abr";
				break;
			case 7:
				mes3 = "Jul";
				mes2 = "Jun";
				mes1 = "Mai";
				break;
			case 8:
				mes3 = "Ago";
				mes2 = "Jul";
				mes1 = "Jun";
				break;
			case 9:
				mes3 = "Set";
				mes2 = "Ago";
				mes1 = "Jul";
				break;
			case 10:
				mes3 = "Out";
				mes2 = "Set";
				mes1 = "Jul";
				break;
			case 11:
				mes3 = "Nov";
				mes2 = "Out";
				mes1 = "Set";
				break;
			case 12:
				mes3 = "Dez";
				mes2 = "Nov";
				mes1 = "Out";
				break;

			default:
				System.out.println("Nao e valido");
			}
		} else
			System.out.println("Valor nao valido");
	}

	public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
		Document pdf = (Document) document;
		pdf.open();
		pdf.setPageSize(PageSize.A4);

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		String logo = externalContext.getRealPath("") + File.separator + "resources" + File.separator + "demo"
				+ File.separator + "images" + File.separator + "prime_logo.png";

		pdf.add(Image.getInstance(logo));
	}

}
package br.gov.pe.pm.esgc.rn;

import java.util.List;

import br.gov.pe.pm.esgc.model.Usuario;
import br.gov.pe.pm.esgc.hibernateDAO.UsuarioHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.UsuarioDAO;
import br.gov.pe.pm.esgc.util.db.DAOFactory;
//import br.gov.pe.pm.esgc.util.method.AleatorioUtil;
//import br.gov.pe.pm.esgc.util.method.EmailUtil;
import br.gov.pe.pm.esgc.util.method.MetodosUtil;

public class UsuarioRn {

	private LogRn logRn = new LogRn();
	private UsuarioDAO usuarioDAO = new UsuarioHibernateDAO();

	public UsuarioRn() {
		this.usuarioDAO = (UsuarioDAO) DAOFactory.criarDAO(this.usuarioDAO);
	}

		
	public Usuario salvar(Usuario objetoAntigo, Usuario usuarioNovo) {
		
		//Usuario objetoNovo = this.usuarioDAO.salvar(usuarioNovo);
		Usuario objetoNovo;
		//incluir um novo usuário
		if (objetoAntigo.getIdUsuario() == null || objetoAntigo.getIdUsuario() == 0) {
			String senha = "PMPE1825";
			usuarioNovo.setSenha(new MetodosUtil().transformarMd5(senha));
			objetoNovo = this.usuarioDAO.salvar(usuarioNovo);
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
		} else {
			//apenas alterar usuário
			objetoNovo = this.usuarioDAO.salvar(usuarioNovo);
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), objetoNovo.toString());
		}
		return objetoNovo;
		
		
	}

	public Usuario atualizarEsqueceuSenha(Usuario usuario) {
		return this.usuarioDAO.atualizarEsqueceuSenha(usuario);
	}

	public void excluir(Usuario objetoAntigo) {
		this.usuarioDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}

	public Usuario carregar(Integer idUsuario) {
		return this.usuarioDAO.carregar(idUsuario);
	}

	public Usuario buscarPorLogin(String login) {
		return this.usuarioDAO.buscarPorLogin(login);
	}

	public Usuario buscarPorEmail(String email) {
		return this.usuarioDAO.buscarPorEmail(email);
	}

	public List<Usuario> listar() {
		return this.usuarioDAO.listar();
	}

}


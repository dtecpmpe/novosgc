package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.Luz;
import br.gov.pe.pm.esgc.model.LuzAnual;

public interface LuzDAO extends Dao {

	public List<Luz> listar();
	public List<Luz> listarAnual();
	
	/*public Luz salvar(Luz luz);
	
	public void excluir(Luz luz);
	
	public List<Luz> listarAtivo();
	
	public Luz carregar(Integer id);*/
}


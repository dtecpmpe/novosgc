package br.gov.pe.pm.esgc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="diretorias", schema= "conta")
public class Diretorias {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_diretoria", nullable = false)
	private Integer idDiretorias;
	
	@Column(name="cod_diretoria")
	private Integer codDiretoria;
	
	@Column
	private String nome;

	@Column
	private String sigla;

}

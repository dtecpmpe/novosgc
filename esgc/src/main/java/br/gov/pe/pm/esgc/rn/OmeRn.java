package br.gov.pe.pm.esgc.rn;


import java.util.List;

import br.gov.pe.pm.esgc.entity.entityview.OmeView;
import br.gov.pe.pm.esgc.hibernateDAO.OmeViewHibernateDao;
import br.gov.pe.pm.esgc.interfaceDAO.OmeViewDao;
import br.gov.pe.pm.esgc.util.db.DAOFactory;


public class OmeRn {

	private OmeViewDao omeViewDAO = new OmeViewHibernateDao();

	public OmeRn() {
		this.omeViewDAO = (OmeViewDao) DAOFactory.criarDAO(this.omeViewDAO);
	}

	public List<OmeView> listarOme() {
		return this.omeViewDAO.listarOme();
	}
	
	public OmeView carregar(Integer id) {
		return this.omeViewDAO.carregar(id);
	}
	
	public List<OmeView> listarPorNome(String nome) {
		return this.omeViewDAO.listarPorNome(nome);
	}
	
}

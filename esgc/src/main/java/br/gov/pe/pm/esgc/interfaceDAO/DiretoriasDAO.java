package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.Diretorias;

public interface DiretoriasDAO extends Dao {

	public List<Diretorias> listar();
	
	
}


package br.gov.pe.pm.esgc.rn;

import java.util.List;

import br.gov.pe.pm.esgc.hibernateDAO.RelGeralAguaHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.RelGeralAguaDAO;
import br.gov.pe.pm.esgc.model.RelGeralAgua;
import br.gov.pe.pm.esgc.util.db.DAOFactory;

public class RelGeralAguaRn {
	
	private RelGeralAguaDAO relGeralAguaDAO = new RelGeralAguaHibernateDAO();

	public RelGeralAguaRn() {
		this.relGeralAguaDAO = (RelGeralAguaDAO) DAOFactory.criarDAO(this.relGeralAguaDAO);
	}
	
	public List<RelGeralAgua> listar() {
		return this.relGeralAguaDAO.listar();
	}
	
}

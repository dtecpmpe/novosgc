package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.TipoFatura;

public interface TipoFaturaDAO extends Dao {

	public List<TipoFatura> listar();
	
	public TipoFatura salvar(TipoFatura tipoFatura);
	
	public void excluir(TipoFatura tipoFatura);
	
	public List<TipoFatura> listarAtivo();
	
	public TipoFatura carregar(Integer id);
}


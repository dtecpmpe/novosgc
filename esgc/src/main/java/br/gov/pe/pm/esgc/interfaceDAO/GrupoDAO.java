package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.Grupo;
import br.gov.pe.pm.esgc.model.TipoConta;


public interface GrupoDAO extends Dao{
	

	
	public List<Grupo> listar();	
	public Grupo salvar(Grupo grupoNovo);	
	public void excluir(Grupo grupo);	
	public List<Grupo> listarAtivo();	
	//public List<Grupo> listarAtivo(Integer tipoConta);	
	public Grupo carregar(Integer id);
	public List<Grupo> listarGrupo(Integer tipoConta);
	public List<Grupo> listarAtivoConta(TipoConta tipoConta);



}

package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.Usuario;

public interface UsuarioDAO extends Dao {
	
	public Usuario salvar(Usuario usuario);
	public Usuario atualizarEsqueceuSenha(Usuario usuario);
	public void excluir(Usuario usuario);
	public Usuario carregar(Integer idUsuario);
	public Usuario buscarPorLogin(String login);
	public Usuario buscarPorEmail(String email);
	public List<Usuario> listar();
	public List<Usuario> listarAtivos();

}

package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.Conta;
import br.gov.pe.pm.esgc.model.Imovel;


public interface ContaDAO extends Dao {

	public List<Conta> listar();	
	public Conta salvar(Conta contaNovo);	
	public void excluir(Conta conta);	
	public List<Conta> listarAtivo();	
	public Conta carregar(Integer id);
	public List<Conta> listarPorImovel(Imovel imovel);	
	//public void excluirPorImovel(Conta conta);
	//public List<DadosContas> buscarPorMatricula();
	public List<Conta> listarPorMatricula(String matricula);
	public Conta carregarNCM(String ncm);

	
	
}


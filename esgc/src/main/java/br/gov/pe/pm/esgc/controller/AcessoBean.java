package br.gov.pe.pm.esgc.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.gov.pe.pm.esgc.model.Usuario;
import br.gov.pe.pm.esgc.rn.LogRn;


@ManagedBean(name = "acessoBean")
@RequestScoped
public class AcessoBean implements Serializable {

	private static final long serialVersionUID = 3300723272820629358L;
	
	public void registrarLog() {
		new LogRn().salvar();		
	}

	public String getLink() {
		Usuario u = new MainBean().getUsuario();

		if (u.getSenha().equals("926cba7e47aac307b1ab5bb9804f622f"))
			return "1;url=/esgc/page/mseg/form_usuario_alt_senha.xhtml";
		else { 			
			return "1;url=/esgc/page/main.xhtml";
		}

	}

}

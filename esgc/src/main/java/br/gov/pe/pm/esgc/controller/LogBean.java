package br.gov.pe.pm.esgc.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.gov.pe.pm.esgc.entity.Log;
import br.gov.pe.pm.esgc.rn.LogRn;


@Named("logBean")
@ViewScoped
public class LogBean implements Serializable{
	
	
	private static final long serialVersionUID = 3653024363086052316L;
	private List<Log> listaLogs;

	public List<Log> getListaLogs() {
		return listaLogs;
	}

	public void setListaLogs(List<Log> listaLogs) {
		this.listaLogs = listaLogs;
	}
	
	// m�todos
	public void listar() {
		this.setListaLogs(new LogRn().listar());
	}
		
}

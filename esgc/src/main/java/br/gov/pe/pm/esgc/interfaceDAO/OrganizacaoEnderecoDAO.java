package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.Imovel;
import br.gov.pe.pm.esgc.model.OrganizacaoEndereco;

public interface OrganizacaoEnderecoDAO extends Dao {

	public OrganizacaoEndereco salvar(OrganizacaoEndereco organizacaoEnderecoNovo);
	public List<OrganizacaoEndereco> listar();
	public void excluir(OrganizacaoEndereco organizacaoEndereco);
	public OrganizacaoEndereco carregar(Integer id);
	//public List<Imovel> listarPorOme(Integer ome);
	public List<OrganizacaoEndereco> listarPorOme(Integer organizacao);
	
	

}

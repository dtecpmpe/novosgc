package br.gov.pe.pm.esgc.rn;

import java.util.List;

import br.gov.pe.pm.esgc.hibernateDAO.ImovelHibernateDAO;
import br.gov.pe.pm.esgc.interfaceDAO.ImovelDAO;
import br.gov.pe.pm.esgc.model.Imovel;
import br.gov.pe.pm.esgc.rn.LogRn;
import br.gov.pe.pm.esgc.util.db.DAOFactory;

public class ImovelRn {
	
	private ImovelDAO imovelDAO = new ImovelHibernateDAO();
	private LogRn logRn = new LogRn();
	
	public ImovelRn() {
		this.imovelDAO = (ImovelDAO) DAOFactory.criarDAO(this.imovelDAO);
	}
	
	public List<Imovel> listar() {
		return this.imovelDAO.listar();
	}
	
	public List<Imovel> listarPorOme(Integer ome) {
		return this.imovelDAO.listarPorOme(ome);
	}
	
	public Imovel salvar(Imovel objetoAntigo, Imovel imovelNovo) {
		Imovel objetoNovo = this.imovelDAO.salvar(imovelNovo);
		if (objetoNovo.getIdImovel() == null)
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
		else
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), objetoNovo.toString());			
		return objetoNovo;
	}	
	
	public void excluir(Imovel objetoAntigo) {
		this.imovelDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}
	
	public Imovel carregar(Integer id) {
		return this.imovelDAO.carregar(id);
	}	
}



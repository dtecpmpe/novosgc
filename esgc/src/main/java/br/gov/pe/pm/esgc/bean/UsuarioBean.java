package br.gov.pe.pm.esgc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
//import javax.ws.rs.GET;

//import org.jboss.jandex.Main;
import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import br.gov.pe.pm.esgc.entity.entityview.OmeView;
import br.gov.pe.pm.esgc.entity.enumAux.PerfilEnum;
import br.gov.pe.pm.esgc.model.Usuario;
import br.gov.pe.pm.esgc.rn.UsuarioRn;
import br.gov.pe.pm.esgc.util.method.MetodosUtil;
//import br.gov.pe.pm.sgpm.model.rn.UsuarioRN;

@Named("usuarioBean")
@ViewScoped

public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = -858561974014184066L;
	private List<Usuario> listaUsuarios;
	private List<SelectItem> listaPerfils;
	private List<SelectItem> listaOrganizacao;
	private Usuario usuario = new Usuario();
	private Usuario usuarioAnterior = new Usuario();
	private String novaSenha;
	private String senhaAnterior;
	private String confirmarSenha;
	private OmeView omeview = new OmeView();
	private String cpfAux;
	
	
	
	public String getCpfAux() {
		return cpfAux;
	}

	public void setCpfAux(String cpfAux) {
		this.cpfAux = cpfAux;
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<SelectItem> getListaPerfils() {
		return listaPerfils;
	}

	public void setListaPerfils(List<SelectItem> listaPerfils) {
		this.listaPerfils = listaPerfils;
	}

	/*
	 * public List<SelectItem> getListaOrganizacao() { return listaOrganizacao; }
	 */
	public void setListaOrganizacao(List<SelectItem> listaOrganizacao) {
		this.listaOrganizacao = listaOrganizacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuarioAnterior = usuario.clone();
		this.usuario = usuario;
	}

	public Usuario getUsuarioAnterior() {
		return usuarioAnterior;
	}

	public void setUsuarioAnterior(Usuario usuarioAnterior) {
		this.usuarioAnterior = usuarioAnterior;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public String getSenhaAnterior() {
		return senhaAnterior;
	}

	public void setSenhaAnterior(String senhaAnterior) {
		this.senhaAnterior = senhaAnterior;
	}

	public String getConfirmarSenha() {
		return confirmarSenha;
	}

	public void setConfirmarSenha(String confirmarSenha) {
		this.confirmarSenha = confirmarSenha;
	}

	public OmeView getOmeview() {
		return omeview;
	}

	public void setOmeview(OmeView omeview) {
		this.omeview = omeview;
	}
	
	
	public void carregarUsuario() {
		this.setUsuario(new UsuarioRn().buscarPorLogin((((SecurityContext) SecurityContextHolder.getContext()).getAuthentication().getName())));
	}

	 public List<SelectItem> getPerfils() { 
		 if (this.listaPerfils == null) {
			 	this.listaPerfils = new ArrayList<SelectItem>(); 
			 	List<PerfilEnum> perfil = new ArrayList<PerfilEnum>(EnumSet.allOf(PerfilEnum.class));
			 	this.listaPerfils.add(new SelectItem(null, "Selecione...")); 
			 	for (PerfilEnum	 p : perfil) { 
			 		this.listaPerfils.add(new SelectItem(p.name(), p.getDescricao())); 
			 		} 
			 	} 
		 return listaPerfils; 
		}
	 
	 
	 
	public UsuarioBean() {
		omeview = new OmeView();
	}
	
	public List<SelectItem> getListaOrganizacao() {
		if (this.listaOrganizacao == null) {
			this.listaOrganizacao = new ArrayList<SelectItem>();			
			this.listaOrganizacao.add(new SelectItem(null, "Selecione..."));
			this.listaOrganizacao.add(new SelectItem(8, "DAL"));
			this.listaOrganizacao.add(new SelectItem(29, "DTEC"));	
			
		}
		return listaOrganizacao;
	}
	

	public void listar() {
		this.setListaUsuarios(new UsuarioRn().listar());
	}

	public String removeAcentos(String s) {
		return new MetodosUtil().removeAcentos(s);
	}

	public void novo() {
		this.usuario = new Usuario();
		usuarioAnterior = new Usuario();
		this.usuario.setAtivo(true);

	}

	public void salvar() {
		String mensagem = "Usuário salvo com sucesso! ";
		if (this.usuario.getIdUsuario() == null) {
			mensagem += "A senha inicial é: PMPE1825.";
		}

		 new UsuarioRn().salvar(this.usuarioAnterior, this.usuario);
		// mensagem += "A senha inicial é: PMPE1825.";

		listar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext.getCurrentInstance().update(Arrays.asList("frm:msgs", "frm:cons_usuario"));
	}

	public String excluir() {
		UsuarioRn usuarioRn = new UsuarioRn();
		usuarioRn.excluir(this.usuario);
		usuarioAnterior = new Usuario();
		listar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuário excluído com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);

		return null;
	}
	
	public String alterarSenha() {

		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage msg;

		if (this.usuario.getSenha().equals(new MetodosUtil().transformarMd5(senhaAnterior))) {

			if (this.novaSenha.equals(this.confirmarSenha)) {

				UsuarioRn usuarioRn = new UsuarioRn();
				this.usuario.setSenha(new MetodosUtil().transformarMd5(this.confirmarSenha));
				usuarioRn.salvar(this.usuarioAnterior, this.usuario);

				msg = new FacesMessage("A senha foi alterada!");
				msg.setSeverity(FacesMessage.SEVERITY_INFO);
				context.addMessage(null, msg);

			} else {

				msg = new FacesMessage("A nova senha não foi confirmada corretamente!");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null, msg);

			}

		} else {
			msg = new FacesMessage("A senha atual não confere!");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, msg);
		}

		this.novaSenha = null;
		this.senhaAnterior = null;
		this.confirmarSenha = null;

		return null;

	}

	public void resetarSenha() {

		String senha = "PMPE1825";
		this.usuario.setSenha(new MetodosUtil().transformarMd5(senha));
		new UsuarioRn().salvar(this.usuarioAnterior, this.usuario);
		listar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"A nova senha para este usuário é: " + senha, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext.getCurrentInstance().update(Arrays.asList("frm:msgs", "frm:cons_usuario"));

	}
	

}

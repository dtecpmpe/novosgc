package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.RelGeralLuz;

public interface RelGeralLuzDAO extends Dao {

	public List<RelGeralLuz> listar();
	
}


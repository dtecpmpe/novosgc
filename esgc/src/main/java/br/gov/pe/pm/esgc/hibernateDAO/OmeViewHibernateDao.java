package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.gov.pe.pm.esgc.entity.entityview.OmeView;
import br.gov.pe.pm.esgc.interfaceDAO.OmeViewDao;

public class OmeViewHibernateDao implements OmeViewDao {

	private Session session;

	private String sql = " SELECT p.id_organizacao, p.ativo, p.nome, p.sigla FROM mrh.organizacao AS p "
			+ " where p.ativo=true and p.id_organizacao not in(80,81,82,88,90,91,92,103,107,200) ";
	private String sqlcarregar = " SELECT id_organizacao, ativo, p.nome, p.sigla,bairro, cep, estado, cidade, complemento, logradouro, numero, ponto_referencia  "		
			+ " FROM mrh.organizacao AS p LEFT JOIN (SELECT * FROM mrh.organizacao_endereco) AS pe USING (id_organizacao) ";

	public void setSession(Session session) {
		this.session = session;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<OmeView> listarOme() {

		this.sql += " ORDER BY p.sigla ";
		Query query = this.session.createSQLQuery(sql);
		List<OmeView> lista = new ArrayList<OmeView>();
		OmeView p;
		List<Object> result = (List<Object>) query.list();
		Iterator itr = result.iterator();

		while (itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			p = new OmeView();
			p.setIdOrganizacao(Integer.parseInt(String.valueOf(obj[0])));
			p.setNome(String.valueOf(obj[2]));
			p.setSigla(String.valueOf(obj[3]));

			lista.add(p);
		}

		return lista;
	}

	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	@Override
	public OmeView carregar(Integer id) {

		this.sqlcarregar += " WHERE p.id_organizacao = " + id;
		Query query = this.session.createSQLQuery(this.sqlcarregar);
		List<Object> result = query.list();
		Iterator itr = result.iterator();
		Object[] obj = (Object[]) itr.next();
		OmeView p = new OmeView();
		p.setIdOrganizacao(Integer.parseInt(String.valueOf(obj[0])));

		if (String.valueOf(obj[1]).equals("null")) {
			p.setAtivo(null);
		} else {
			p.setAtivo(Boolean.parseBoolean(String.valueOf(obj[1])));
		}
		if (String.valueOf(obj[2]).equals("null")) {
			p.setNome(null);
		} else {
			p.setNome(String.valueOf(obj[2]));
		}
		if (String.valueOf(obj[3]).equals("null")) {
			p.setSigla(null);
		} else {
			p.setSigla(String.valueOf(obj[3]));
		}
		if (String.valueOf(obj[5]).equals("null")) {
			p.setCep(null);
		} else {
			p.setCep(String.valueOf(obj[5]));
		}
		if (String.valueOf(obj[8]).equals("null")) {
			p.setLogradouro(null);
		} else {
			p.setLogradouro(String.valueOf(obj[8]));
		}
		if (String.valueOf(obj[9]).equals("null")) {
			p.setNumero(null);
		} else {
			p.setNumero(Integer.parseInt(String.valueOf(obj[9])));
		}
		if (String.valueOf(obj[4]).equals("null")) {
			p.setBairro(null);
		} else {
			p.setBairro(String.valueOf(obj[4]));
		}
		if (String.valueOf(obj[6]).equals("null")) {
			p.setCidade(null);
		} else {
			p.setCidade(String.valueOf(obj[6]));
		}
		if (String.valueOf(obj[7]).equals("null")) {
			p.setComplemento(null);
		} else {
			p.setComplemento(String.valueOf(obj[7]));
		}
		if (String.valueOf(obj[10]).equals("null")) {
			p.setPontoReferencia(null);
		} else {
			p.setPontoReferencia(String.valueOf(obj[10]));
		}

		return p;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<OmeView> listarPorNome(String nome) {

		this.sql += " AND p.nome = " + nome;
		Query query = this.session.createSQLQuery(sql);
		List<OmeView> lista = new ArrayList<OmeView>();
		OmeView p;
		List<Object> result = (List<Object>) query.list();
		Iterator itr = result.iterator();

		while (itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			p = new OmeView();
			p.setNome(String.valueOf(obj[2]));
			lista.add(p);
		}

		return lista;
	}

}

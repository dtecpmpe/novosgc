package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import br.gov.pe.pm.esgc.model.Usuario;
import br.gov.pe.pm.esgc.interfaceDAO.UsuarioDAO;




public class UsuarioHibernateDAO implements UsuarioDAO {
	
	private Session session;
	private Criteria c;
	
	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public Usuario salvar(Usuario usuario) {
		return (Usuario) this.session.merge(usuario);
	}
	
	@Override
	public Usuario atualizarEsqueceuSenha(Usuario usuario) {
		this.session.update(usuario);
		return usuario;
	}

	@Override
	public void excluir(Usuario usuario) {
		this.session.delete(usuario);
	}

	@Override
	public Usuario carregar(Integer idUsuario) {
		return (Usuario) this.session.get(Usuario.class, idUsuario);
	}

	@Override
	public Usuario buscarPorLogin(String login) {
		/*String hql = "select u from Usuario u where u.login = :login";
		Query consulta = this.session.createQuery(hql);
		consulta.setString("login", login);
		return (Usuario) consulta.uniqueResult();*/
		prepararCriteria();
		this.c.add(Restrictions.eq("login", login));
		return (Usuario) this.c.uniqueResult();
		//Query query = session.createQuery("from Usuario u join fetch u.organizacao where u.login = :login");
		//query.setParameter("login", login);
		//return (Usuario) query.uniqueResult();
		
	}
	
	@Override
	public Usuario buscarPorEmail(String email) {
		
		/*String hql = "select u from Usuario u where u.email = :email";
		Query consulta = this.session.createQuery(hql);
		consulta.setString("email", email);
		return (Usuario) consulta.uniqueResult();*/
		
		
		
		/*Query query = manager.createQuery(
				"from Veiculo where anoFabricacao >= :ano and valor <= :preco");
				query.setParameter("ano", 2009);
				query.setParameter("preco", new BigDecimal(60_000));
				List veiculos = query.getResultList();*/
		
		prepararCriteria();
		this.c.add(Restrictions.eq("email", email));
		return (Usuario) this.c.uniqueResult();
		//Query query = session.createQuery("from Usuario u join fetch u.organizacao o fetch o.organizacaoEndereco where u.email = :email");
		//query.setParameter("email", email);
		//return (Usuario) query.uniqueResult();
		
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> listar() {
		prepararCriteria();
		this.c.addOrder(Order.asc("nome"));
		return this.c.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> listarAtivos() {
		prepararCriteria();
		this.c.addOrder(Order.asc("nome"));
		this.c.add(Restrictions.like("ativo", Boolean.TRUE));
		return this.c.list();
	}

	private void prepararCriteria() {
		
		// para resolver o problema do N+1 usamos essa solução, mas temos mais 2
		// outras que estão no projeto de teste chamado TesteJpa

		this.c = session.createCriteria(Usuario.class, "usuario");
		//this.c.createAlias("usuario.organizacao", "organizacao", JoinType.LEFT_OUTER_JOIN);
	//	this.c.createAlias("organizacao.organizacaoEndereco", "organizacaoEndereco", JoinType.LEFT_OUTER_JOIN);
		
	}

	

}

package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.esgc.interfaceDAO.AguaDAO;
import br.gov.pe.pm.esgc.model.Agua;
import br.gov.pe.pm.esgc.model.AguaAnual;

public class AguaHibernateDAO implements AguaDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Agua> listar() {	
		return (List<Agua>)session.createCriteria(Agua.class, "agua").list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Agua> listarAnual() {	
		return (List<Agua>)session.createCriteria(AguaAnual.class, "agua").list();
	}

	/*@Override
	public Agua salvar(Agua agua) {
		return (Agua)this.session.merge(agua);
	
		
	}

	@Override
	public void excluir(Agua agua) {
		this.session.delete(agua);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Agua> listarAtivo() {
		return (List<Agua>)session.createCriteria(Agua.class, "agua").add(Restrictions.eq("ativo", true)).list();
	}
	
	public Agua carregar(Integer id) {
		return (Agua)session.createCriteria(Agua.class, "agua").add(Restrictions.eq("matricula", id)).uniqueResult();
	}*/
}
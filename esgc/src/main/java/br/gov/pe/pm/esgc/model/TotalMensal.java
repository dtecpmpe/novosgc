package br.gov.pe.pm.esgc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="total_mensal", schema = "conta")
public class TotalMensal {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id_total_mensal", nullable = false )
	private Integer idTotalMensal;
	
	@Column
	private String tipo;
	
	@Column 
	private String competencia;
	
	@Column
	private String consumoMetropolitano;
	
	@Column
	private String consumoInterior;
	
	@Column
	private String consumoTotal;
	
	@Column
	private String valorMetropolitano;
	
	@Column
	private String valorInterior;
	
	@Column
	private String valorTotal;
	
	@Column
	private Integer qtdRegistro;
	
	@Column
	private Integer qtdContMat;
	
	@Column
	private String usuario;
	
	@Column
	private Date data;
	
	
}
package br.gov.pe.pm.esgc.util.method;


import java.math.BigInteger;
import java.security.MessageDigest;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

public class MetodosUtil {

	public String removeAcentos(String s) {
		if (s == null) {
			return "";
		}
		String semAcentos = s.toLowerCase();
		semAcentos = semAcentos.replaceAll("[áŕâăä]", "a");
		semAcentos = semAcentos.replaceAll("[éčęëê]", "e");
		semAcentos = semAcentos.replaceAll("[íěîď]", "i");
		semAcentos = semAcentos.replaceAll("[óňôőö]", "o");
		semAcentos = semAcentos.replaceAll("[úůűü]", "u");
		semAcentos = semAcentos.replaceAll("ç", "c");
		semAcentos = semAcentos.replaceAll("ń", "n");
		return semAcentos;
	}

	public String transformarMd5(String texto) {

		try {
			String textoMd5 = texto;
			MessageDigest msg = MessageDigest.getInstance("MD5");
			msg.update(textoMd5.getBytes(), 0, textoMd5.length());
			textoMd5 = new BigInteger(1, msg.digest()).toString(16);
			while (textoMd5.length() < 32) 
				textoMd5 = 0 + textoMd5;
			return textoMd5;
		} catch (Exception e) {
			//System.out.println("Erro ao realizar MD5. Erro: " + e.getMessage());
			return "Erro no MD5";
		}

	}

	public static void executarJavaScript(String comando) {
		RequestContext.getCurrentInstance().execute(comando);
	}

	public static void criarAviso(String txt) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, txt, txt);
		FacesContext.getCurrentInstance().addMessage(txt, msg);
	}

}


package br.gov.pe.pm.esgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.esgc.model.TipoConta;

public interface TipoContaDAO extends Dao {

	public List<TipoConta> listar();	
	public TipoConta salvar(TipoConta tipoConta);	
	public void excluir(TipoConta tipoConta);	
	public List<TipoConta> listarAtivos();	
	public TipoConta carregar(Integer id);

		
	
}


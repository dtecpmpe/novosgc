package br.gov.pe.pm.esgc.util.db;

import br.gov.pe.pm.esgc.interfaceDAO.Dao;

public class DAOFactory {
	
	public static Dao criarDAO(Object o) {
		Dao dao = (Dao) o;
		dao.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return dao;
	}
	
}
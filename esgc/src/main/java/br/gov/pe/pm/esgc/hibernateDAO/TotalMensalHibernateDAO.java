package br.gov.pe.pm.esgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.esgc.interfaceDAO.TotalMensalDAO;
import br.gov.pe.pm.esgc.model.TotalMensal;

public class TotalMensalHibernateDAO implements TotalMensalDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TotalMensal> listar() {	
		return (List<TotalMensal>)session.createCriteria(TotalMensal.class, "totalMensal").list();
	}

	@Override
	public TotalMensal salvar(TotalMensal totalMensal) {
		return (TotalMensal)this.session.merge(totalMensal);
	
		
	}

	@Override
	public void excluir(TotalMensal totalMensal) {
		this.session.delete(totalMensal);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TotalMensal> listarAtivo() {
		return (List<TotalMensal>)session.createCriteria(TotalMensal.class, "totalMensal").add(Restrictions.eq("ativo", true)).list();
	}
	
	public TotalMensal carregar(Integer id) {
		return (TotalMensal)session.createCriteria(TotalMensal.class, "totalMensal").add(Restrictions.eq("matricula", id)).uniqueResult();
	}
}
package br.gov.pe.pm.sgc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.gov.pe.pm.sgc.model.Grupo;
import br.gov.pe.pm.sgc.model.TipoConta;
import br.gov.pe.pm.sgc.rn.GrupoRn;
import br.gov.pe.pm.sgc.rn.TipoContaRn;

import br.gov.pe.pm.sgc.util.method.MetodosUtil;

@Named("grupoBean")
@ViewScoped
public class GrupoBean implements Serializable {

	
	private static final long serialVersionUID = -2057784558659533746L;
	private List<Grupo> listaGrupo;
	private Grupo grupo = new Grupo();
	private Grupo grupoAnterior = new Grupo();

	public List<Grupo> getListaGrupo() {
		return listaGrupo;
	}

	public void setListaGrupo(List<Grupo> listaGrupo) {
		this.listaGrupo = listaGrupo;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupoAnterior = grupo.clone();
		this.grupo = grupo;
	}

	public Grupo getGrupoAnterior() {
		return grupoAnterior;
	}

	public void setGrupoAnterior(Grupo grupoAnterior) {
		this.grupoAnterior = grupoAnterior;
	}

	
	
	
	//métodos
	

	public void listar() {
		this.setListaGrupo(new GrupoRn().listar());
	}

	public String removeAcentos(String s) {
		return new MetodosUtil().removeAcentos(s);
	}

	public void novo() {
		this.grupo = new Grupo();
		grupoAnterior = new Grupo();
		this.grupo.setStatus(true);
	}

	public String excluir() {
		new GrupoRn().excluir(this.grupo);
		grupoAnterior = new Grupo();
		listar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}
	public String editar(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", id);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("call", "1");
		return "grupos?faces-redirect=true";
				
	}
	
	public void salvar() {
		this.grupo.setStatus(true);
		this.grupo = new GrupoRn().salvar(this.grupoAnterior, this.grupo);
		listar();
		// mensagem de sucesso
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext.getCurrentInstance().update(Arrays.asList("frm:msgs", "frm:grupo"));
	}

	public List<SelectItem> getListarTipoContas() {
		
		List<SelectItem> listaTipoConta = new ArrayList<SelectItem>();	
		List<TipoConta> tipoContas = new TipoContaRn().listarAtivos();
		listaTipoConta.add(new SelectItem(null, "Selecione..."));
		for (TipoConta tc : tipoContas) {
			listaTipoConta.add(new SelectItem(tc.getIdTipoConta(), tc.getDescricao()));

		}
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@ " + listaTipoConta.get(0).getLabel());
		return listaTipoConta;
	}
	

	/*
	 * public List<SelectItem> getListarGrupos() { if (this.listaGrupo == null) {
	 * this.listaGrupo = new ArrayList<SelectItem>(); List<Grupos> pv = new
	 * GruposRn().listarGrupos(); this.listaGrupo.add(new SelectItem(null,
	 * "Selecione...")); for (Grupos p : pv) { this.listaGrupo.add(new
	 * SelectItem(p.getIdGrupo(), p.getIdTipoConta())); }
	 * 
	 * } return listaGrupo; }
	 */

	public GrupoBean() {
	
		
	}

	
	
	
}

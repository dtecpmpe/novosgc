package br.gov.pe.pm.sgc.bean;


import javax.faces.bean.RequestScoped;
import javax.inject.Named;

@Named("consumoBean")
@RequestScoped
public class ConsumoBean {
	
	private Integer valorA;
	private Integer valorB;
	private Integer resultado;
	
	
	public Integer getValorA() {
		return valorA;
	}
	public void setValorA(Integer valorA) {
		this.valorA = valorA;
	}
	public Integer getValorB() {
		return valorB;
	}
	public void setValorB(Integer valorB) {
		this.valorB = valorB;
	}
	public Integer getResultado() {
		return resultado;
	}
	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}
	
	public void soma() {
		this.resultado = this.valorA + this.valorB;
		
		
	}
	

}

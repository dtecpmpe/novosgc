package br.gov.pe.pm.sgc.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.gov.pe.pm.sgc.model.TipoFatura;
import br.gov.pe.pm.sgc.rn.TipoFaturaRn;
import br.gov.pe.pm.sgc.util.method.MetodosUtil;





@Named("tipoFaturaBean")
@ViewScoped

public class TipoFaturaBean implements Serializable {

	
	private static final long serialVersionUID = -2547051358660306708L;
	
	private List<TipoFatura> listaTipoFatura;
	private TipoFatura tipoFatura = new TipoFatura();
	private TipoFatura tipoFaturaAnterior = new TipoFatura();;

	
	public List<TipoFatura> getlistaTipoFatura() {
		return listaTipoFatura;
	}
	public void setListaTipoFatura(List<TipoFatura> listaTipoFatura) {
		this.listaTipoFatura = listaTipoFatura;
	
	}
	public TipoFatura getTipoFatura() {
		return tipoFatura;
	}
	public void setTipoFatura(TipoFatura tipoFatura) {
		this.tipoFaturaAnterior = tipoFatura.clone();
		this.tipoFatura = tipoFatura;
	}
	
	
	public TipoFatura getTipoFaturaAnterior() {
		return tipoFaturaAnterior;
	}
	public void setTipoFaturaAnterior(TipoFatura tipoFaturaAnterior) {
		this.tipoFaturaAnterior = tipoFaturaAnterior;
	}
	
 //MÉTODOS
	
	public void listar() {
		this.setListaTipoFatura(new TipoFaturaRn().listar());
	}
	
	public String removeAcentos(String s) {
		return new MetodosUtil().removeAcentos(s);
	}

	
	
	public void novo() {
		this.tipoFatura = new TipoFatura();
		tipoFaturaAnterior = new TipoFatura();
		this.tipoFatura.setStatus(true);
	}
	
	public String excluir() {
		new TipoFaturaRn().excluir(this.tipoFatura);
		tipoFaturaAnterior = new TipoFatura();
		listar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}
	
	public void salvar() {
		new TipoFaturaRn().salvar(this.tipoFaturaAnterior, this.tipoFatura);
		listar();
		// mensagem de sucesso
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext.getCurrentInstance().update(Arrays.asList("frm:msgs", "frm:tipo_fatura"));
	}
	
	
}
package br.gov.pe.pm.sgc.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import br.gov.pe.pm.sgc.model.TipoConta;
import br.gov.pe.pm.sgc.rn.TipoContaRn;
import br.gov.pe.pm.sgc.util.method.MetodosUtil;




@Named("tipoContasBean")
@ViewScoped

public class TipoContasBean implements Serializable {

	
	private static final long serialVersionUID = -2547051358660306708L;
	
	public TipoContasBean () {}
	private List<TipoConta> listaTipoConta;
	private TipoConta tipoConta = new TipoConta();
	private TipoConta tipoContaAnterior = new TipoConta();

	
	
	public List<TipoConta> getListaTipoConta() {
		return listaTipoConta;
	}
	public void setListaTipoConta(List<TipoConta> listaTipoConta) {
		this.listaTipoConta = listaTipoConta;
	}
	public TipoConta getTipoConta() {
		return tipoConta;
	}
	public void setTipoConta(TipoConta tipoConta) {
		this.tipoContaAnterior = tipoConta.clone();
		this.tipoConta = tipoConta;
	}
	
	
	public TipoConta getTipoContaAnterior() {
		return tipoContaAnterior;
	}
	public void setTipoContaAnterior(TipoConta tipoContaAnterior) {
		this.tipoContaAnterior = tipoContaAnterior;
	}
	
 //MÉTODOS
	
	
	
	public void listar() {
		this.setListaTipoConta(new TipoContaRn().listar());
	}
	
	public String removeAcentos(String s) {
		return new MetodosUtil().removeAcentos(s);
	}

	
	
	public void novo() {
		this.tipoConta = new TipoConta();
		tipoContaAnterior = new TipoConta();
		this.tipoConta.setStatus(true);
		
	}
	
	public String excluir() {
		new TipoContaRn().excluir(this.tipoConta);
		tipoContaAnterior = new TipoConta();
		listar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro excluído com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return null;
	}
	
	public void salvar() {
		new TipoContaRn().salvar(this.tipoContaAnterior, this.tipoConta);
		listar();
		// mensagem de sucesso
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso!", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext.getCurrentInstance().update(Arrays.asList("frm:msgs", "frm:tipo_conta"));
	}
	
	
}
package br.gov.pe.pm.sgc.rn;

import java.util.List;

import br.gov.pe.pm.sgc.hibernateDAO.TipoFaturaHibernateDAO;
import br.gov.pe.pm.sgc.interfaceDAO.TipoFaturaDAO;
import br.gov.pe.pm.sgc.model.TipoFatura;
import br.gov.pe.pm.sgc.util.db.DAOFactory;


public class TipoFaturaRn {
	
	private TipoFaturaDAO tipoFaturaDAO = new TipoFaturaHibernateDAO();
	private LogRn logRn = new LogRn();

	public TipoFaturaDAO getTipoFaturaDAO() {
		return tipoFaturaDAO;
	}

	public void setTipoFaturaDAO(TipoFaturaDAO tipoFaturaDAO) {
		this.tipoFaturaDAO = tipoFaturaDAO;
	}

	public TipoFaturaRn() {
		this.tipoFaturaDAO = (TipoFaturaDAO) DAOFactory.criarDAO(this.tipoFaturaDAO);
	}
	
	public List<TipoFatura> listar() {
		return this.tipoFaturaDAO.listar();
	}

	public void excluir(TipoFatura objetoAntigo) {
		this.tipoFaturaDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}
	public void salvar(TipoFatura objetoAntigo, TipoFatura tipoFaturaNovo) {
		TipoFatura objetoNovo = this.tipoFaturaDAO.salvar(tipoFaturaNovo);		
		 if (objetoNovo.getIdTipoFatura() == null)
			 this.logRn.salvar(objetoNovo.getClass().getSimpleName(), "",objetoNovo.toString()); 
		 else
			 this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), objetoNovo.toString());
	}
	
	public TipoFatura carregar(Integer id) {
		return this.tipoFaturaDAO.carregar(id);
	}
	
}

package br.gov.pe.pm.sgc.rn;

import java.util.List;

import br.gov.pe.pm.sgc.hibernateDAO.AguaHibernateDAO;
import br.gov.pe.pm.sgc.interfaceDAO.AguaDAO;
import br.gov.pe.pm.sgc.model.Agua;
import br.gov.pe.pm.sgc.util.db.DAOFactory;

public class AguaRn {
	
	private AguaDAO aguaDAO = new AguaHibernateDAO();

	public AguaRn() {
		this.aguaDAO = (AguaDAO) DAOFactory.criarDAO(this.aguaDAO);
	}
	
	public List<Agua> listar() {
		return this.aguaDAO.listar();
	}
	
}

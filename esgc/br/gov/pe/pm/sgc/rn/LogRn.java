package br.gov.pe.pm.sgc.rn;

import java.util.Date;
import java.util.List;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import br.gov.pe.pm.sgc.entity.Log;
import br.gov.pe.pm.sgc.entity.enumAux.AcaoLogEnum;
import br.gov.pe.pm.sgc.hibernateDAO.LogDAOHibernate;
import br.gov.pe.pm.sgc.interfaceDAO.LogDAO;
import br.gov.pe.pm.sgc.util.db.DAOFactory;

public class LogRn {
	private LogDAO logDAO = new LogDAOHibernate();
	private Log log = new Log();

	public LogRn() {
		this.logDAO = (LogDAO) DAOFactory.criarDAO(logDAO);
	}

	public void salvar(String nomeTabela, String valorAntigo, String valorNovo) {
		// log = this.logDAO.carregarMaiorId();
		log.setUsuario(((SecurityContext) SecurityContextHolder.getContext()).getAuthentication().getName());
		log.setDataAlteracao(new Date());
		if (valorAntigo.equals("") && !valorNovo.equals(""))
			log.setAcao(AcaoLogEnum.I);
		else if (!valorAntigo.equals("") && valorNovo.equals(""))
			log.setAcao(AcaoLogEnum.D);
		else if (!valorAntigo.equals("") && !valorNovo.equals(""))
			log.setAcao(AcaoLogEnum.U);
		log.setNomeTabela(nomeTabela);
		log.setValorAntigo(valorAntigo);
		log.setValorNovo(valorNovo);

		this.logDAO.salvar(log);
	}
	
	public void salvar() {
		// log = this.logDAO.carregarMaiorId();
		log.setUsuario(((SecurityContext) SecurityContextHolder.getContext()).getAuthentication().getName());
		log.setDataAlteracao(new Date());
		log.setAcao(AcaoLogEnum.A);
		log.setNomeTabela(null);
		log.setValorAntigo(null);
		log.setValorNovo(null);

		this.logDAO.salvar(log);
	}

	public List<Log> listar() {
		return this.logDAO.listar();
	}

}

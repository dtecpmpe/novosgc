package br.gov.pe.pm.sgc.rn;

import java.util.List;

import br.gov.pe.pm.sgc.hibernateDAO.LuzHibernateDAO;
import br.gov.pe.pm.sgc.interfaceDAO.LuzDAO;
import br.gov.pe.pm.sgc.model.Luz;
import br.gov.pe.pm.sgc.util.db.DAOFactory;

public class LuzRn {
	
	private LuzDAO luzDAO = new LuzHibernateDAO();

	public LuzRn() {
		this.luzDAO = (LuzDAO) DAOFactory.criarDAO(this.luzDAO);
	}
	
	public List<Luz> listar() {
		return this.luzDAO.listar();
	}
	
}

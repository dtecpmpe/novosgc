package br.gov.pe.pm.sgc.rn;

import java.util.List;

import br.gov.pe.pm.sgc.entity.Usuario;
import br.gov.pe.pm.sgc.hibernateDAO.UsuarioDAOHibernate;
import br.gov.pe.pm.sgc.interfaceDAO.UsuarioDAO;
import br.gov.pe.pm.sgc.util.db.DAOFactory;
import br.gov.pe.pm.sgc.util.method.MetodosUtil;

public class UsuarioRn {

	private LogRn logRn = new LogRn();
	private UsuarioDAO usuarioDAO = new UsuarioDAOHibernate();

	public UsuarioRn() {
		this.usuarioDAO = (UsuarioDAO) DAOFactory.criarDAO(this.usuarioDAO);
	}

	/*public String salvar(Usuario usuario) {
		String msg;
		Integer idUsuario = usuario.getIdUsuario();
		// verifica se é Salvar ou Alterar para poder enviar e-mail
		if (idUsuario == null || idUsuario == 0) {
			// verifica a existência do usuário pelo login
			Usuario usuarioAux = new UsuarioRN().buscarPorLogin(usuario.getLogin());
			if (usuarioAux == null) {
				// verifica se o e-mail já está cadastrado
				usuarioAux = new UsuarioRN().buscarPorEmail(usuario.getEmail());
				if (usuarioAux == null) {
					String senha = AleatorioUtil.randomString(8);
					usuario.setSenha(new MetodosUtil().transformarMd5(senha));
					this.usuarioDAO.salvar(usuario);
					EmailUtil emailUtil = new EmailUtil();
					emailUtil.emailCadastro(usuario.getEmail(), usuario.getLogin(), senha);
					msg = "Usuário salvo com sucesso! A senha inicial foi enviada para o e-mail cadastrado.";
				} else {
					msg = "Este e-mail já está cadastrado!";
				}

			} else {
				msg = "Este login já está cadastrado!";
			}
		} else {
			// aqui é o ponto que apenas atualiza os dados do usuário sem reemitir a senha
			this.usuarioDAO.salvar(usuario);
			msg = "Usuário atualizado com sucesso!";
		}
		return msg;
		
	}*/
	
	public Usuario salvar(Usuario objetoAntigo, Usuario usuarioNovo) {
		
		//Usuario objetoNovo = this.usuarioDAO.salvar(usuarioNovo);
		Usuario objetoNovo;
		//incluir um novo usuário
		/*if (objetoAntigo.getIdUsuario() == null || objetoAntigo.getIdUsuario() == 0) {
			String senha = AleatorioUtil.randomString(8);
			usuarioNovo.setSenha(new MetodosUtil().transformarMd5(senha));
			objetoNovo = this.usuarioDAO.salvar(usuarioNovo);
			this.logRN.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
			new EmailUtil().emailCadastro(objetoNovo.getEmail(), objetoNovo.getLogin(), senha);
		} else {
			//apenas alterar usuário
			objetoNovo = this.usuarioDAO.salvar(usuarioNovo);
			this.logRN.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), objetoNovo.toString());
		}*/
		
		if (objetoAntigo.getIdUsuario() == null || objetoAntigo.getIdUsuario() == 0) {
			String senha = "PMPE1825";
			usuarioNovo.setSenha(new MetodosUtil().transformarMd5(senha));
			objetoNovo = this.usuarioDAO.salvar(usuarioNovo);
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
		} else {
			//apenas alterar usuário
			objetoNovo = this.usuarioDAO.salvar(usuarioNovo);
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), objetoNovo.toString());
		}
		return objetoNovo;
		
	}

	public Usuario atualizarEsqueceuSenha(Usuario usuario) {
		return this.usuarioDAO.atualizarEsqueceuSenha(usuario);
	}

	public void excluir(Usuario objetoAntigo) {
		this.usuarioDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}

	public Usuario carregar(Integer idUsuario) {
		return this.usuarioDAO.carregar(idUsuario);
	}

	public Usuario buscarPorLogin(String login) {
		return this.usuarioDAO.buscarPorLogin(login);
	}

	public Usuario buscarPorEmail(String email) {
		return this.usuarioDAO.buscarPorEmail(email);
	}

	public List<Usuario> listar() {
		return this.usuarioDAO.listar();
	}

}


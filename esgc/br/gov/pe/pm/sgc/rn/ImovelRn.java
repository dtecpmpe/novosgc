package br.gov.pe.pm.sgc.rn;


import java.util.List;

//import br.gov.pe.pm.sgc.entity.entityview.OmeView;
import br.gov.pe.pm.sgc.hibernateDAO.ImovelHibernateDAO;
import br.gov.pe.pm.sgc.interfaceDAO.ImovelDAO;

import br.gov.pe.pm.sgc.model.Imovel;
import br.gov.pe.pm.sgc.util.db.DAOFactory;
import br.gov.pe.pm.sgc.rn.LogRn;

public class ImovelRn {
	
	private ImovelDAO imovelDAO = new ImovelHibernateDAO();
	private LogRn logRn = new LogRn();
	
	public ImovelRn() {
		this.imovelDAO = (ImovelDAO) DAOFactory.criarDAO(this.imovelDAO);
	}
	
	public List<Imovel> listar() {
		return this.imovelDAO.listar();
	}
	public List<Imovel> listarPorOme(Integer ome) {
		return this.imovelDAO.listarPorOme(ome);
	}
	public Imovel salvar(Imovel objetoAntigo, Imovel imovelNovo) {
		Imovel objetoNovo = this.imovelDAO.salvar(imovelNovo);
		if (objetoNovo.getIdImovel() == null)
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
		else
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), objetoNovo.toString());
			
		return objetoNovo;
	}
	
	
	
	public void excluir(Imovel objetoAntigo) {
		this.imovelDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}
	public void excluirPorOme(Imovel imovel) {
		this.imovelDAO.excluirPorOme(imovel);
	}
	public Imovel carregar(Integer id) {
		return this.imovelDAO.carregar(id);
	}
	
	
}



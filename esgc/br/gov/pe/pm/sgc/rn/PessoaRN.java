package br.gov.pe.pm.sgc.rn;

import java.util.List;

import br.gov.pe.pm.sgc.entity.Pessoa;
import br.gov.pe.pm.sgc.hibernateDAO.PessoaDAOHibernate;
import br.gov.pe.pm.sgc.interfaceDAO.PessoaDAO;
import br.gov.pe.pm.sgc.util.db.DAOFactory;

public class PessoaRN {

	private LogRn logRN = new LogRn();
	private PessoaDAO pessoaDAO = new PessoaDAOHibernate();

	public PessoaRN() {
		this.pessoaDAO = (PessoaDAO) DAOFactory.criarDAO(this.pessoaDAO);
	}

	public Pessoa salvar(String objetoAntigo, Pessoa pessoaNovo) {
		Pessoa objetoNovo = this.pessoaDAO.salvar(pessoaNovo);

		if (pessoaNovo.getIdPessoa() == null)
			this.logRN.salvar(objetoNovo.getClass().getSimpleName(), "", objetoNovo.toString());
		else
			this.logRN.salvar(objetoNovo.getClass().getSimpleName(), objetoAntigo, objetoNovo.toString());
		
		return objetoNovo;

		// this.pessoaDAO.salvar(pessoa);
	}

	public void excluir(Pessoa objetoAntigo) {
		this.pessoaDAO.excluir(objetoAntigo);
		this.logRN.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");

	}

	public Pessoa carregar(Integer idPessoa) {
		return this.pessoaDAO.carregar(idPessoa);
	}
	
	public Pessoa carregarPorCpf(String cpf) {
		return this.pessoaDAO.carregarPorCpf(cpf);
	}

	public List<Pessoa> listar() {
		return this.pessoaDAO.listar();
	}

	public List<Pessoa> listarPorNome(String nomeCompleto) {
		return this.pessoaDAO.listarPorNome(nomeCompleto);
	}

	public List<Pessoa> listarPorCpf(String cpf) {
		return this.pessoaDAO.listarPorCPF(cpf);
	}

}

package br.gov.pe.pm.sgc.rn;

import java.util.List;

import br.gov.pe.pm.sgc.hibernateDAO.ContaHibernateDAO;
import br.gov.pe.pm.sgc.interfaceDAO.ContaDAO;
import br.gov.pe.pm.sgc.model.Conta;
import br.gov.pe.pm.sgc.model.Imovel;
import br.gov.pe.pm.sgc.util.db.DAOFactory;

public class ContaRn {
	
	private ContaDAO contaDAO = new ContaHibernateDAO();
	private LogRn logRn = new LogRn();
	public ContaRn() {
		this.contaDAO = (ContaDAO) DAOFactory.criarDAO(this.contaDAO);
	}
	public List<Conta> listarPorImovel(Imovel imovel) {
		return this.contaDAO.listarPorImovel(imovel);
	}
	public List<Conta> listar() {
		return this.contaDAO.listar();
	}

	public Conta salvar(Conta objetoAntigo, Conta contaNovo) {
		Conta objetoNovo = this.contaDAO.salvar(contaNovo);
		if (objetoNovo.getIdConta() != null)
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), "", objetoNovo.toString());
		else
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(),objetoNovo.toString());
		return objetoNovo;
	}
	
	
	public void excluir(Conta objetoAntigo) {
		this.contaDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}
	public Conta carregar(Integer id) {
		return this.contaDAO.carregar(id);
	}
	public List<Conta> listarPorMatricula(String matricula){
		return this.contaDAO.listarPorMatricula(matricula);
	}
	public Conta carregarNCM(String ncm) {
		// TODO Auto-generated method stub
		return this.contaDAO.carregarNCM(ncm);
	}
	//public void excluir(Conta conta) {
		// TODO Auto-generated method stub
		
	//}
}

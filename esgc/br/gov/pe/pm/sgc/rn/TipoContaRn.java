package br.gov.pe.pm.sgc.rn;

import java.util.List;

import br.gov.pe.pm.sgc.hibernateDAO.TipoContaHibernateDAO;
import br.gov.pe.pm.sgc.interfaceDAO.TipoContaDAO;
import br.gov.pe.pm.sgc.model.TipoConta;
import br.gov.pe.pm.sgc.util.db.DAOFactory;




public class TipoContaRn {
	
	private TipoContaDAO tipoContaDAO = new TipoContaHibernateDAO();
	private LogRn logRn = new LogRn();
	public TipoContaRn() {
		this.tipoContaDAO = (TipoContaDAO) DAOFactory.criarDAO(this.tipoContaDAO);
	}
	
	public List<TipoConta> listar() {
		return this.tipoContaDAO.listar();
	}
	public void excluir(TipoConta objetoAntigo) {
		this.tipoContaDAO.excluir(objetoAntigo);
		this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
	}
	public void salvar(TipoConta objetoAntigo, TipoConta tipoContaNova) {
		TipoConta objetoNovo = this.tipoContaDAO.salvar( tipoContaNova);
		if (objetoNovo.getIdTipoConta() == null)
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(), "");
		else
			this.logRn.salvar(objetoAntigo.getClass().getSimpleName(), objetoAntigo.toString(),objetoNovo.toString());
			
	}
	
	public List<TipoConta> listarAtivos() {
		return this.tipoContaDAO.listarAtivos();
		
	}
	/*
	 * public List<TipoConta> listarAtivo(Integer idTipoConta) { return
	 * this.tipoContaDAO.listarAtivo(idTipoConta); }
	 */

	public TipoConta carregar(Integer id) {
		// TODO Auto-generated method stub
		return (TipoConta)this.tipoContaDAO.carregar(id);
	}
	
	
}

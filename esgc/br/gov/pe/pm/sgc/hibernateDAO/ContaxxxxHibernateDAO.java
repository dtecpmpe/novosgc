package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.sgc.interfaceDAO.ContaxxxxDAO;
import br.gov.pe.pm.sgc.model.Contaxxxx;

public class ContaxxxxHibernateDAO implements ContaxxxxDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Contaxxxx> listar() {	
		return (List<Contaxxxx>)session.createCriteria(Contaxxxx.class, "conta").list();
	}

	@Override
	public Contaxxxx salvar(Contaxxxx conta) {
		return (Contaxxxx)this.session.merge(conta);
	
		
	}

	@Override
	public void excluir(Contaxxxx conta) {
		this.session.delete(conta);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Contaxxxx> listarAtivo() {
		return (List<Contaxxxx>)session.createCriteria(Contaxxxx.class, "Conta").add(Restrictions.eq("ativo", true)).list();
	}
	
	public Contaxxxx carregar(Integer id) {
		return (Contaxxxx)session.createCriteria(Contaxxxx.class, "conta").add(Restrictions.eq("idConta", id)).uniqueResult();
	}
}
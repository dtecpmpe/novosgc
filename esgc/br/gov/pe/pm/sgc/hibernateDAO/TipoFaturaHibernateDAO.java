package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.sgc.interfaceDAO.TipoFaturaDAO;
import br.gov.pe.pm.sgc.model.TipoFatura;

public class TipoFaturaHibernateDAO implements TipoFaturaDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TipoFatura> listar() {	
		return (List<TipoFatura>)session.createCriteria(TipoFatura.class, "tipoFatura").list();
	}

	@Override
	public TipoFatura salvar(TipoFatura tipoFatura) {
		return (TipoFatura)this.session.merge(tipoFatura);
	
		
	}

	@Override
	public void excluir(TipoFatura tipoFatura) {
		this.session.delete(tipoFatura);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TipoFatura> listarAtivo() {
		return (List<TipoFatura>)session.createCriteria(TipoFatura.class, "tipoFatura").add(Restrictions.eq("status", true)).list();
	}
	
	public TipoFatura carregar(Integer id) {
		return (TipoFatura)session.createCriteria(TipoFatura.class, "tipoFatura").add(Restrictions.eq("idTipoConta", id)).uniqueResult();
	}
}
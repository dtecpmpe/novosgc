package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;


import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.sgc.interfaceDAO.TipoContaDAO;
import br.gov.pe.pm.sgc.model.TipoConta;

public class TipoContaHibernateDAO implements TipoContaDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TipoConta> listar() {	
		return (List<TipoConta>)session.createCriteria(TipoConta.class, "tipo_conta").list();
	}

	@Override
	public TipoConta salvar(TipoConta tipoConta) {
		return (TipoConta)this.session.merge(tipoConta);
	
		
	}

	@Override
	public void excluir(TipoConta tipoConta) {
		this.session.delete(tipoConta);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TipoConta> listarAtivos() {		
		return (List<TipoConta>)session.createCriteria(TipoConta.class, "tipo_conta").add(Restrictions.eq("status", true)).list();
				
	}
	
	
	
	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public List<TipoConta> listarAtivo(Integer idTipoConta) { return
	 * (List<TipoConta>)session.createCriteria(TipoConta.class,
	 * "tipoConta").add(Restrictions.eq("status", true)).list(); }
	 */
	
	public TipoConta carregar(Integer id) {
		return (TipoConta)session.createCriteria(TipoConta.class, "tipo_conta").add(Restrictions.eq("idTipoConta", id)).uniqueResult();
	}

	
	
}
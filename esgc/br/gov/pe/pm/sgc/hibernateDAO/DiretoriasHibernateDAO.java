package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;

import br.gov.pe.pm.sgc.interfaceDAO.DiretoriasDAO;
import br.gov.pe.pm.sgc.model.Diretorias;

public class DiretoriasHibernateDAO implements DiretoriasDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Diretorias> listar() {	
		return (List<Diretorias>)session.createCriteria(Diretorias.class, "diretoria").list();
	}

	
}
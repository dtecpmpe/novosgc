package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.sgc.interfaceDAO.LuzDAO;
import br.gov.pe.pm.sgc.model.Luz;

public class LuzHibernateDAO implements LuzDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Luz> listar() {	
		return (List<Luz>)session.createCriteria(Luz.class, "luz").list();
	}

	@Override
	public Luz salvar(Luz luz) {
		return (Luz)this.session.merge(luz);
	
		
	}

	@Override
	public void excluir(Luz luz) {
		this.session.delete(luz);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Luz> listarAtivo() {
		return (List<Luz>)session.createCriteria(Luz.class, "luz").add(Restrictions.eq("ativo", true)).list();
	}
	
	public Luz carregar(Integer id) {
		return (Luz)session.createCriteria(Luz.class, "luz").add(Restrictions.eq("contrato", id)).uniqueResult();
	}
}
package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.sgc.model.Grupo;
import br.gov.pe.pm.sgc.model.TipoConta;
import br.gov.pe.pm.sgc.interfaceDAO.GrupoDAO;



public class GrupoHibernateDAO implements GrupoDAO{
	
	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Grupo> listar() {	
		return (List<Grupo>)session.createCriteria(Grupo.class, "grupo").list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Grupo> listarGrupos() {	
		return (List<Grupo>)session.createCriteria(Grupo.class, "grupo").list();
	}

	@Override
	public Grupo salvar(Grupo grupo) {
	  return (Grupo)this.session.merge(grupo);	
	}
	

	@Override
	public void excluir(Grupo grupo) {
		this.session.delete(grupo);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Grupo> listarAtivo() {
		return (List<Grupo>)session.createCriteria(Grupo.class, "grupo").add(Restrictions.eq("status", true)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Grupo> listarAtivoConta(TipoConta tipoConta) {
		return (List<Grupo>)session.createCriteria(Grupo.class, "grupo").add(Restrictions.eq("tipoConta",tipoConta)).add(Restrictions.eq("status", true)).list();
	}
	@Override
	public Grupo carregar(Integer id) {
		return (Grupo)session.createCriteria(Grupo.class, "grupo").add(Restrictions.eq("id_grupo", id)).uniqueResult();
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Grupo> listarGrupo(Integer tipoConta) {
		return (List<Grupo>)session.createCriteria(Grupo.class, "grupo").add(Restrictions.eq("tipoConta.idTipoConta", tipoConta)).list();
	}
	
	
}



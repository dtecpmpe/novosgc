package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.sgc.entity.Pessoa;
import br.gov.pe.pm.sgc.interfaceDAO.PessoaDAO;


public class PessoaDAOHibernate implements PessoaDAO {

	private Session session;
	private Criteria c;

	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public Pessoa salvar(Pessoa pessoa) {
		return (Pessoa) this.session.merge(pessoa);
	}

	@Override
	public void excluir(Pessoa pessoa) {
		this.session.delete(pessoa);
	}

	@Override
	public Pessoa carregar(Integer idPessoa) {
		prepararCriteria();
		return (Pessoa) this.c.add(Restrictions.eq("idPessoa", idPessoa)).uniqueResult();
		//return (Pessoa) this.session.get(Pessoa.class, idPessoa);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pessoa> listar() {
		prepararCriteria();
		return this.c.addOrder(Order.asc("nomeCompleto")).list();
		// return this.session.createCriteria(Pessoa.class).addOrder(Order.asc("nomeCompleto")).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pessoa> listarPorNome(String nomeCompleto) {
		prepararCriteria();
		this.c.addOrder(Order.asc("nomeCompleto"));
		this.c.add(Restrictions.like("nomeCompleto", nomeCompleto + "%"));
		return this.c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pessoa> listarPorCPF(String cpf) {
		prepararCriteria();
		this.c.addOrder(Order.asc("cpf"));
		this.c.add(Restrictions.like("cpf", cpf + "%"));
		return this.c.list();
	}

	@Override
	public Pessoa carregarPorCpf(String cpf) {
		prepararCriteria();
		this.c.add(Restrictions.eq("cpf", cpf));
		return (Pessoa) this.c.uniqueResult();
	}

	private void prepararCriteria() {
		
		// para resolver o problema do N+1 usamos essa solução, mas temos mais 2
		// outras que estão no projeto de teste chamado TesteJpa

		this.c = session.createCriteria(Pessoa.class, "pessoa");
		/*this.c.createAlias("pessoa.pessoaEndereco", "pessoaEndereco", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.pessoaDocumentoObrigatorio", "pessoaDocumentoObrigatorio", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.pessoaMedida", "pessoaMedida", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.pessoaCaracFisica", "pessoaCaracFisica", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.pessoaContato", "pessoaContato", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.pessoaDadosBancario", "pessoaDadosBancario", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoaDadosBancario.banco", "banco", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.pessoaImagem", "pessoaImagem", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.pessoaDocumentoComplementar", "pessoaDocumentoComplementar", JoinType.LEFT_OUTER_JOIN);*/

		/*this.c.createAlias("pessoa.documentoFuncional", "documentoFuncional", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("documentoFuncional.orgao", "orgao", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("documentoFuncional.documentoOficial", "documentoOficial", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("documentoOficial.tipoDocumento", "tipoDocumento", JoinType.LEFT_OUTER_JOIN);*/
		
		/*this.c.createAlias("pessoa.escolaridade", "escolaridade", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.estadoCivil", "estadoCivil", JoinType.LEFT_OUTER_JOIN);
		this.c.createAlias("pessoa.religiao", "religiao", JoinType.LEFT_OUTER_JOIN);*/
		
	}

}
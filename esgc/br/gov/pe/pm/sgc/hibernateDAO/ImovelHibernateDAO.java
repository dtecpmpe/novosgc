package br.gov.pe.pm.sgc.hibernateDAO;



import java.util.List;


import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.sgc.interfaceDAO.ImovelDAO;
import br.gov.pe.pm.sgc.model.Imovel;

public class ImovelHibernateDAO implements ImovelDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Imovel> listar() {	
		return (List<Imovel>)session.createCriteria(Imovel.class, "imovel").list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Imovel> listarPorOme(Integer ome) {	
		return (List<Imovel>)session.createCriteria(Imovel.class, "imovel").add(Restrictions.eq("idOme", ome)).list();
	}

	@Override
	public Imovel salvar(Imovel imovel) {
		return (Imovel)this.session.merge(imovel);	
	}

	@Override
	public void excluir(Imovel imovel) {
		this.session.delete(imovel);
		
	}
	@Override
	public void excluirPorOme(Imovel imovel) {
		this.session.delete(imovel);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Imovel> listarAtivo() {
		return (List<Imovel>)session.createCriteria(Imovel.class, "imovel").add(Restrictions.eq("status", true)).list();
	}
	
	public Imovel carregar(Integer id) {
		return (Imovel)session.createCriteria(Imovel.class, "imovel").add(Restrictions.eq("idImovel", id)).uniqueResult();
	}
}

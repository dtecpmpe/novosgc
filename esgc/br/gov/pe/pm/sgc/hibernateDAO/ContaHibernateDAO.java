package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.gov.pe.pm.sgc.interfaceDAO.ContaDAO;
import br.gov.pe.pm.sgc.model.Conta;
import br.gov.pe.pm.sgc.model.Imovel;


public class ContaHibernateDAO implements ContaDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Conta> listar() {	
		return (List<Conta>)session.createCriteria(Conta.class, "conta").list();
	}

	@Override
	public Conta salvar(Conta conta) {
		return (Conta)this.session.merge(conta);
	
		
	}

	@Override
	public void excluir(Conta conta) {
		this.session.delete(conta);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Conta> listarAtivo() {
		return (List<Conta>)session.createCriteria(Conta.class, "conta").add(Restrictions.eq("status", true)).list();
	}
	
	public Conta carregar(Integer id) {
		return (Conta)session.createCriteria(Conta.class, "conta").add(Restrictions.eq("idConta", id)).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Conta> listarPorImovel(Imovel imovel) {		
		return (List<Conta>)session.createCriteria(Conta.class, "conta").add(Restrictions.eq("imovel", imovel)).list();
	}

	/*
	 * @Override public void excluirPorImovel(Conta conta) { // TODO Auto-generated
	 * method stub
	 * 
	 * }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Conta> listarPorMatricula(String matricula) {	
		return (List<Conta>)session.createCriteria(Conta.class, "conta").add(Restrictions.like("nContratoMatricula", matricula+"%")).list();
	}

	/*
	 * @Override public List<DadosContas> buscarPorMatricula() { // TODO
	 * Auto-generated method stub return null; }
	 */

	@Override
	public Conta carregarNCM(String ncm) {
		// TODO Auto-generated method stub
		return (Conta)session.createCriteria(Conta.class, "conta").add(Restrictions.eq("nContratoMatricula", ncm)).uniqueResult();
	}
	
	
}
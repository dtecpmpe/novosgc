package br.gov.pe.pm.sgc.hibernateDAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;

import br.gov.pe.pm.sgc.entity.Log;
import br.gov.pe.pm.sgc.interfaceDAO.LogDAO;

public class LogDAOHibernate implements LogDAO {
	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	public Log salvar(Log log){
		return (Log) this.session.merge(log);
	}
	
/*	public Log carregarMaiorId() {
		this.session.flush();
		return (Log) session.createSQLQuery("select * FROM mseg.log l where id_log = (SELECT Max(id_log) FROM mseg.log)").addEntity(Log.class).uniqueResult();
	}
	*/
	@SuppressWarnings("unchecked")
	@Override
	public List<Log> listar() {

		return this.session.createCriteria(Log.class).addOrder(Order.desc("dataAlteracao")).list();
	}
}

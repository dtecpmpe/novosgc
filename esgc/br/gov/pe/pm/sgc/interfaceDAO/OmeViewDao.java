package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.sgc.entity.entityview.OmeView;


public interface OmeViewDao extends Dao {
	

	public OmeView carregar(Integer id);
	public List<OmeView> listarPorNome(String nome);
	public List<OmeView> listarOme();
	
	
}

package br.gov.pe.pm.sgc.interfaceDAO;



import br.gov.pe.pm.sgc.entity.entityview.ImovelView;



public interface ImovelViewDao extends Dao {
	

	public ImovelView carregar(Integer id);
	
	
}

package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.sgc.model.Diretorias;

public interface DiretoriasDAO extends Dao {

	public List<Diretorias> listar();
	
	
}


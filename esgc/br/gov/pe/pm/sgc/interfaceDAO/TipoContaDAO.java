package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.List;
import br.gov.pe.pm.sgc.model.TipoConta;

public interface TipoContaDAO extends Dao {

	public List<TipoConta> listar();	
	public TipoConta salvar(TipoConta tipoConta);	
	public void excluir(TipoConta tipoConta);	
	public List<TipoConta> listarAtivos();	
	public TipoConta carregar(Integer id);

		
	
}


package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.sgc.model.TipoFatura;

public interface TipoFaturaDAO extends Dao {

	public List<TipoFatura> listar();
	
	public TipoFatura salvar(TipoFatura tipoFatura);
	
	public void excluir(TipoFatura tipoFatura);
	
	public List<TipoFatura> listarAtivo();
	
	public TipoFatura carregar(Integer id);
}


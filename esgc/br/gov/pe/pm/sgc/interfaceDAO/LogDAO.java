package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.sgc.entity.Log;


public interface LogDAO extends Dao {

	public Log salvar(Log log);
/*	public Log carregarMaiorId();*/
	public List<Log> listar();
	
}

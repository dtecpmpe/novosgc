package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.sgc.model.Luz;

public interface LuzDAO extends Dao {

	public List<Luz> listar();
	
	public Luz salvar(Luz luz);
	
	public void excluir(Luz luz);
	
	public List<Luz> listarAtivo();
	
	public Luz carregar(Integer id);
}


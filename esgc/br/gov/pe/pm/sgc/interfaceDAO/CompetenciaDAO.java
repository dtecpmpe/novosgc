package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.Date;
import java.util.List;

import br.gov.pe.pm.sgc.model.Competencia;
import br.gov.pe.pm.sgc.model.Conta;
import br.gov.pe.pm.sgc.model.TipoConta;

public interface CompetenciaDAO extends Dao {

	public List<Competencia> listar();
	
	public List<Competencia> listar(TipoConta tipoConta);
	
	public List<Competencia> listar(TipoConta tipoConta,  Date competencia);
	
	public List<Competencia> listar(TipoConta tipoConta,  Date competencia, Conta conta);
	
	public Competencia salvar(Competencia competencia);
	
	public void excluir(Competencia competencia);
	
	public List<Competencia> listarAtivo();
	
	public Competencia carregar(Integer id);
}


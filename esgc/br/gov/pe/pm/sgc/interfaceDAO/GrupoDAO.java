package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.sgc.model.Grupo;
import br.gov.pe.pm.sgc.model.TipoConta;


public interface GrupoDAO extends Dao{
	

	
	public List<Grupo> listar();	
	public Grupo salvar(Grupo grupoNovo);	
	public void excluir(Grupo grupo);	
	public List<Grupo> listarAtivo();	
	//public List<Grupo> listarAtivo(Integer tipoConta);	
	public Grupo carregar(Integer id);
	public List<Grupo> listarGrupo(Integer tipoConta);
	public List<Grupo> listarAtivoConta(TipoConta tipoConta);



}

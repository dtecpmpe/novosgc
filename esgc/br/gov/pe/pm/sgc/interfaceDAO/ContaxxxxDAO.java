package br.gov.pe.pm.sgc.interfaceDAO;

import java.util.List;

import br.gov.pe.pm.sgc.model.Contaxxxx;

public interface ContaxxxxDAO extends Dao {

	public List<Contaxxxx> listar();
	
	public Contaxxxx salvar(Contaxxxx conta);
	
	public void excluir(Contaxxxx conta);
	
	public List<Contaxxxx> listarAtivo();
	
	public Contaxxxx carregar(Integer id);
}


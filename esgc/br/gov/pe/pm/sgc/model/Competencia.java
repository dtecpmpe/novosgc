package br.gov.pe.pm.sgc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="competencia", schema="conta")
public class Competencia  implements Serializable, Cloneable {
	
	
	private static final long serialVersionUID = -7871305090921329511L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_competencia", nullable = false)
	private Integer idCompetencia;
	
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_conta", foreignKey = @ForeignKey(name = "competencia_conta_fkey") )
	private Conta conta;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date competencia;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date vencimento;
	
		
	@Column(length = 6, precision = 2)
	private Double consumo;
	
	@Column(length = 6, precision = 2)
	private Double valor;
	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipo_fatura", foreignKey = @ForeignKey(name = "competencia_contas_id_tipo_fatura_fkey") )
	private TipoFatura tipoFatura;

	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_conta", foreignKey = @ForeignKey(name = "competencia_contas_id_tipo_conta_fkey") )
	private TipoConta tipoConta;
	
	@OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
	@JoinColumn(name = "id_imovel", foreignKey = @ForeignKey(name = "competencia_id_imovel_fkey") )
	private Imovel imovel;

	
	public Competencia() {
		this.tipoConta = new TipoConta();
		this.tipoFatura = new TipoFatura();
		this.imovel = new Imovel();
		this.conta = new Conta();
	}


	public Integer getIdCompetencia() {
		return idCompetencia;
	}


	public void setIdCompetencia(Integer idCompetencia) {
		this.idCompetencia = idCompetencia;
	}


	public Conta getConta() {
		return conta;
	}


	public void setConta(Conta conta) {
		this.conta = conta;
	}


	public Date getCompetencia() {
		return competencia;
	}

	public void setCompetencia(Date competencia) {
		//this.competencia = new Date();
		this.competencia = competencia;
	}


	public Date getVencimento() {
		return vencimento;
	}


	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}


	public Double getConsumo() {
		return consumo;
	}


	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}


	public Double getValor() {
		return valor;
	}


	public void setValor(Double valor) {
		this.valor = valor;
	}


	public TipoFatura getTipoFatura() {
		return tipoFatura;
	}


	public void setTipoFatura(TipoFatura tipoFatura) {
		this.tipoFatura = tipoFatura;
	}


	public TipoConta getTipoConta() {
		return tipoConta;
	}


	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public Imovel getImovel() {
		return imovel;
	}


	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((competencia == null) ? 0 : competencia.hashCode());
		result = prime * result + ((consumo == null) ? 0 : consumo.hashCode());
		result = prime * result + ((conta == null) ? 0 : conta.hashCode());
		result = prime * result + ((idCompetencia == null) ? 0 : idCompetencia.hashCode());
		result = prime * result + ((imovel == null) ? 0 : imovel.hashCode());
		result = prime * result + ((tipoConta == null) ? 0 : tipoConta.hashCode());
		result = prime * result + ((tipoFatura == null) ? 0 : tipoFatura.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		result = prime * result + ((vencimento == null) ? 0 : vencimento.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Competencia other = (Competencia) obj;
		if (competencia == null) {
			if (other.competencia != null)
				return false;
		} else if (!competencia.equals(other.competencia))
			return false;
		if (consumo == null) {
			if (other.consumo != null)
				return false;
		} else if (!consumo.equals(other.consumo))
			return false;
		if (conta == null) {
			if (other.conta != null)
				return false;
		} else if (!conta.equals(other.conta))
			return false;
		if (idCompetencia == null) {
			if (other.idCompetencia != null)
				return false;
		} else if (!idCompetencia.equals(other.idCompetencia))
			return false;
		if (imovel == null) {
			if (other.imovel != null)
				return false;
		} else if (!imovel.equals(other.imovel))
			return false;
		if (tipoConta == null) {
			if (other.tipoConta != null)
				return false;
		} else if (!tipoConta.equals(other.tipoConta))
			return false;
		if (tipoFatura == null) {
			if (other.tipoFatura != null)
				return false;
		} else if (!tipoFatura.equals(other.tipoFatura))
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		if (vencimento == null) {
			if (other.vencimento != null)
				return false;
		} else if (!vencimento.equals(other.vencimento))
			return false;
		return true;
	}


	@Override
	public Competencia clone() {
		try {
			return (Competencia)super.clone();
		} catch (CloneNotSupportedException e) {
			//System.out.println("O objeto CIDADE não pode ser clonado." + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public String toString() {
		return "idCompetencia= " + idCompetencia + ", conta= " + conta + ", competencia=" + competencia + 
				", vencimento= " + vencimento + ", consumo= "+ consumo + ", valor= "+ valor + 
				", tipoFatura= "+ tipoFatura +  ", tipoConta= "+ tipoConta + ", imovel= "+ imovel + ".";
	}
		

	
}
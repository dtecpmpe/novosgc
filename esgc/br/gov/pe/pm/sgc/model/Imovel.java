package br.gov.pe.pm.sgc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.pe.pm.sgc.rn.OmeRn;

@Entity
@Table(name="imovel", schema = "conta")
public class Imovel implements Serializable, Cloneable {

	private static final long serialVersionUID = 260886080964254883L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id_imovel", nullable = false )
	private Integer idImovel;
	
	@Column
	private String cep;
	
	@Column
	private String nome;
	
	@Column
	private String logradouro;
	
	@Column
	private Integer numero;
	
	@Column
	private String bairro;
	
	@Column
	private String cidade;
	
	@Column
	private String complemento;
	
	@Column
	private String telefone;
	
	@Column(name = "ponto_referencia")
	private String pontoReferencia;
	
	@Column(name="id_organizacao")
	private Integer idOme;
	
	@Column
	private Boolean status;
	
	
	public Integer getIdImovel() {
		return idImovel;
	}
	public void setIdImovel(Integer idImovel) {
		this.idImovel = idImovel;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getPontoReferencia() {
		return pontoReferencia;
	}
	public void setPontoReferencia(String pontoReferencia) {
		this.pontoReferencia = pontoReferencia;
	}
	public Integer getIdOme() {
		return idOme;
	}
	public void setIdOme(Integer idOme) {
		this.idOme = idOme;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((complemento == null) ? 0 : complemento.hashCode());
		result = prime * result + ((idImovel == null) ? 0 : idImovel.hashCode());
		result = prime * result + ((idOme == null) ? 0 : idOme.hashCode());
		result = prime * result + ((logradouro == null) ? 0 : logradouro.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((pontoReferencia == null) ? 0 : pontoReferencia.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Imovel other = (Imovel) obj;
		if (bairro == null) {
			if (other.bairro != null)
				return false;
		} else if (!bairro.equals(other.bairro))
			return false;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (complemento == null) {
			if (other.complemento != null)
				return false;
		} else if (!complemento.equals(other.complemento))
			return false;
		if (idImovel == null) {
			if (other.idImovel != null)
				return false;
		} else if (!idImovel.equals(other.idImovel))
			return false;
		if (idOme == null) {
			if (other.idOme != null)
				return false;
		} else if (!idOme.equals(other.idOme))
			return false;
		if (logradouro == null) {
			if (other.logradouro != null)
				return false;
		} else if (!logradouro.equals(other.logradouro))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (pontoReferencia == null) {
			if (other.pontoReferencia != null)
				return false;
		} else if (!pontoReferencia.equals(other.pontoReferencia))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}
	
	@Override
	public Imovel clone() {
		try {
			return (Imovel)super.clone();
		} catch (CloneNotSupportedException e) {
			//System.out.println("O objeto CIDADE não pode ser clonado." + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public String toString() {
		return "idImovel= " + idImovel + ", cep= " + cep + ", nome= " + nome + ", logradouro= " + logradouro + 
				", numero= " + numero + ", bairro= " + bairro + ", cidade= " + cidade + ", complemento= " +
				complemento + ", telefone= " + telefone + ", pontoReferencia= " + pontoReferencia+ ", idOme= "+
				idOme+ ", status= " + status + ".";
	}
	
	public String getOme() {
		return new OmeRn().carregar(idOme).getSigla();
	}
	

}

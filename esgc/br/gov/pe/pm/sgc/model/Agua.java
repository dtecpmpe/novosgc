package br.gov.pe.pm.sgc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="relatorio_agua1", schema="conta")
public class Agua {
	
	@Column
	private String	ome;
	@Id
	@Column(name="matricula")
	private String matricula;
	@Column(name="cidade")
	private String cidade;
	@Column(name="endereco")
	private String endereco;
	@Column(name="mes3_ano_anterior")
	private double mes3AnoAnterior;
	@Column(name="mes2_ano_anterior")
	private double mes2AnoAnterior;
	@Column(name="mes1_ano_anterior")
	private double mes1AnoAnterior;
	@Column(name="mes3_ano_atual")
	private double mes3AnoAtual;
	@Column(name="mes2_ano_atual")
	private double mes2AnoAtual;
	@Column(name="mes1_ano_atual")
	private double mes1AnoAtual;
	@Column(name="media_trimetre_anterior")
	private double mediaTrimetreAnterior;
	@Column(name="media_trimetre_atual")
	private double mediaTrimetreAtual;
	@Column(name="status")
	private String status;
	@Column(name="meta_trimestral")
	private Double metaTrimestral;
	
	/*public boolean isAumentouAtual() {
		if(mediaTrimetreAnterior-mediaTrimetreAtual<0) return true;
		else return false;
	}*/
	
	public String getMeta() {
		String retorno = status;
		if (!metaTrimestral.equals(0.0)) {
			retorno += " " + metaTrimestral + "%";
		}
		return retorno;
	}

	public String getOme() {
		return ome;
	}

	public void setOme(String ome) {
		this.ome = ome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public double getMes3AnoAnterior() {
		return mes3AnoAnterior;
	}

	public void setMes3AnoAnterior(double mes3AnoAnterior) {
		this.mes3AnoAnterior = mes3AnoAnterior;
	}

	public double getMes2AnoAnterior() {
		return mes2AnoAnterior;
	}

	public void setMes2AnoAnterior(double mes2AnoAnterior) {
		this.mes2AnoAnterior = mes2AnoAnterior;
	}

	public double getMes1AnoAnterior() {
		return mes1AnoAnterior;
	}

	public void setMes1AnoAnterior(double mes1AnoAnterior) {
		this.mes1AnoAnterior = mes1AnoAnterior;
	}

	public double getMes3AnoAtual() {
		return mes3AnoAtual;
	}

	public void setMes3AnoAtual(double mes3AnoAtual) {
		this.mes3AnoAtual = mes3AnoAtual;
	}

	public double getMes2AnoAtual() {
		return mes2AnoAtual;
	}

	public void setMes2AnoAtual(double mes2AnoAtual) {
		this.mes2AnoAtual = mes2AnoAtual;
	}

	public double getMes1AnoAtual() {
		return mes1AnoAtual;
	}

	public void setMes1AnoAtual(double mes1AnoAtual) {
		this.mes1AnoAtual = mes1AnoAtual;
	}

	public double getMediaTrimetreAnterior() {
		return mediaTrimetreAnterior;
	}

	public void setMediaTrimetreAnterior(double mediaTrimetreAnterior) {
		this.mediaTrimetreAnterior = mediaTrimetreAnterior;
	}

	public double getMediaTrimetreAtual() {
		return mediaTrimetreAtual;
	}

	public void setMediaTrimetreAtual(double mediaTrimetreAtual) {
		this.mediaTrimetreAtual = mediaTrimetreAtual;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getMetaTrimestral() {
		return metaTrimestral;
	}

	public void setMetaTrimestral(Double metaTrimestral) {
		this.metaTrimestral = metaTrimestral;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		long temp;
		temp = Double.doubleToLongBits(mediaTrimetreAnterior);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(mediaTrimetreAtual);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(mes1AnoAnterior);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(mes1AnoAtual);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(mes2AnoAnterior);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(mes2AnoAtual);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(mes3AnoAnterior);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(mes3AnoAtual);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((metaTrimestral == null) ? 0 : metaTrimestral.hashCode());
		result = prime * result + ((ome == null) ? 0 : ome.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agua other = (Agua) obj;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (Double.doubleToLongBits(mediaTrimetreAnterior) != Double.doubleToLongBits(other.mediaTrimetreAnterior))
			return false;
		if (Double.doubleToLongBits(mediaTrimetreAtual) != Double.doubleToLongBits(other.mediaTrimetreAtual))
			return false;
		if (Double.doubleToLongBits(mes1AnoAnterior) != Double.doubleToLongBits(other.mes1AnoAnterior))
			return false;
		if (Double.doubleToLongBits(mes1AnoAtual) != Double.doubleToLongBits(other.mes1AnoAtual))
			return false;
		if (Double.doubleToLongBits(mes2AnoAnterior) != Double.doubleToLongBits(other.mes2AnoAnterior))
			return false;
		if (Double.doubleToLongBits(mes2AnoAtual) != Double.doubleToLongBits(other.mes2AnoAtual))
			return false;
		if (Double.doubleToLongBits(mes3AnoAnterior) != Double.doubleToLongBits(other.mes3AnoAnterior))
			return false;
		if (Double.doubleToLongBits(mes3AnoAtual) != Double.doubleToLongBits(other.mes3AnoAtual))
			return false;
		if (metaTrimestral == null) {
			if (other.metaTrimestral != null)
				return false;
		} else if (!metaTrimestral.equals(other.metaTrimestral))
			return false;
		if (ome == null) {
			if (other.ome != null)
				return false;
		} else if (!ome.equals(other.ome))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
}

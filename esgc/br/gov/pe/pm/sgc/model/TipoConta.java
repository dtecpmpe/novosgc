package br.gov.pe.pm.sgc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipo_conta", schema = "conta")
public class TipoConta implements Serializable, Cloneable {
	
	private static final long serialVersionUID = -7053975620741424964L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id_tipo_conta", nullable = false )
	private Integer idTipoConta;	
	
	@Column
	private String descricao;

	@Column
	private Boolean status;

	public Integer getIdTipoConta() {
		return idTipoConta;
	}

	public void setIdTipoConta(Integer idTipoConta) {
		this.idTipoConta = idTipoConta;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((idTipoConta == null) ? 0 : idTipoConta.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoConta other = (TipoConta) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (idTipoConta == null) {
			if (other.idTipoConta != null)
				return false;
		} else if (!idTipoConta.equals(other.idTipoConta))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public TipoConta clone() {
		try {
			return (TipoConta)super.clone();
		} catch (CloneNotSupportedException e) {
			//System.out.println("O objeto CIDADE não pode ser clonado." + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public String toString() {
		return "idTipoConta= " + idTipoConta + ", descricao= " + descricao + ", status=" + status +  ".";
	}
	
	
}
	
package br.gov.pe.pm.sgc.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="conta", schema = "conta")
public class Contaxxxx {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id_conta", nullable = false )
	private Integer idConta;
	
	@Column
	private Boolean ativo;
	
	@Column
	private String tipoConta;
	
	@Column
	private String tipoFatura;
	
	@Column(name="id_competencia")
	private Integer	competencia;
	
	@Column(name="id_cont_mat")
	private Integer cont_mat;
	
	@Column
	private String usuario;
	
	@Column
	private Date data;	
		
	@Column
	private String descricao;
	
	@Column(name="id_grupo")
	private Integer idGrupo;

	
	@Column(name="id_imovel")
	private Integer idImovel;
			
	public Integer getIdConta() {
		return idConta;
	}


	public void setIdConta(Integer idConta) {
		this.idConta = idConta;
	}


	public Boolean getAtivo() {
		return ativo;
	}


	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}


	public String getTipoConta() {
		return tipoConta;
	}


	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}


	public String getTipoFatura() {
		return tipoFatura;
	}


	public void setTipoFatura(String tipoFatura) {
		this.tipoFatura = tipoFatura;
	}


	public Integer getCompetencia() {
		return competencia;
	}


	public void setCompetencia(Integer competencia) {
		this.competencia = competencia;
	}


	public Integer getCont_mat() {
		return cont_mat;
	}


	public void setCont_mat(Integer cont_mat) {
		this.cont_mat = cont_mat;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public Date getData() {
		return data;
	}


	public void setData(Date data) {
		this.data = data;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public Integer getIdGrupo() {
		return idGrupo;
	}


	public void setIdGrupo(Integer idGrupo) {
		this.idGrupo = idGrupo;
	}


	public Integer getIdImovel() {
		return idImovel;
	}


	public void setIdImovel(Integer idImovel) {
		this.idImovel = idImovel;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result + ((competencia == null) ? 0 : competencia.hashCode());
		result = prime * result + ((cont_mat == null) ? 0 : cont_mat.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((idConta == null) ? 0 : idConta.hashCode());
		result = prime * result + ((idGrupo == null) ? 0 : idGrupo.hashCode());
		result = prime * result + ((idImovel == null) ? 0 : idImovel.hashCode());
		result = prime * result + ((tipoConta == null) ? 0 : tipoConta.hashCode());
		result = prime * result + ((tipoFatura == null) ? 0 : tipoFatura.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contaxxxx other = (Contaxxxx) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (competencia == null) {
			if (other.competencia != null)
				return false;
		} else if (!competencia.equals(other.competencia))
			return false;
		if (cont_mat == null) {
			if (other.cont_mat != null)
				return false;
		} else if (!cont_mat.equals(other.cont_mat))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (idConta == null) {
			if (other.idConta != null)
				return false;
		} else if (!idConta.equals(other.idConta))
			return false;
		if (idGrupo == null) {
			if (other.idGrupo != null)
				return false;
		} else if (!idGrupo.equals(other.idGrupo))
			return false;
		if (idImovel == null) {
			if (other.idImovel != null)
				return false;
		} else if (!idImovel.equals(other.idImovel))
			return false;
		if (tipoConta == null) {
			if (other.tipoConta != null)
				return false;
		} else if (!tipoConta.equals(other.tipoConta))
			return false;
		if (tipoFatura == null) {
			if (other.tipoFatura != null)
				return false;
		} else if (!tipoFatura.equals(other.tipoFatura))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}


}

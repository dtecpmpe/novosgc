package br.gov.pe.pm.sgc.entity.enumAux;

public enum AcaoLogEnum {
	I("INSERIR"),U("ATUALIZAR"),D("DELETAR"),A("ACESSO");
	
	// VARI�VEIS
		private String descricao;

		// GETTERS E SETTERS
		AcaoLogEnum(String descricao) {
			this.descricao = descricao;
		}
		
		public String getDescricao() {
			return descricao;
		}
}

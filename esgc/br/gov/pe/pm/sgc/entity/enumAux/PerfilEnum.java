package br.gov.pe.pm.sgc.entity.enumAux;

public enum PerfilEnum {
		
	ADMINISTRADOR("ADMINISTRADOR"),
	MADOME("MRH"), OME("P1"), CPO("CPO"), 
	CPP("CPP"), DGP1("DGP1"),  DGP2("DGP2"),  DGP3("DGP3"),
	DGP5("DGP5"),  DGP8("DGP8"),  DGP9("DGP9"),MAD("MAD"), 
	CONSULTA("CONSULTA"), MB("MATERIAL BÉLICO"), RMB("ARMÁRIA"),
	OMEMB("P1 E MATERIAL BÉLICO"),  OMERMB("P1 E ARMÁRIA"),CONSULTABASICA("CONSULTABASICA"),DPO("DPO"), AUT("AUTORIDADE");
		
		// VARIAVEIS
		private String descricao;

		// GETTERS E SETTERS
		PerfilEnum(String descricao) {
			this.descricao = descricao;
		}
		
		public String getDescricao() {
			return descricao;
		}
			
	}
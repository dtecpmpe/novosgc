package br.gov.pe.pm.sgc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pessoa", schema = "mrh")
public class Pessoa implements Serializable, Cloneable {

	private static final long serialVersionUID = -4783573626353603360L;

	// VARIÁVEIS
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pessoa", nullable = false)
	private Integer idPessoa;

	@Column(name = "cod_ref", length = 20)
	private String codRef;

	@Column(name = "nome_foto", length = 256)
	private String nomeFoto;

	@Column(length = 20, unique = true)
	private String cpf;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_nasc")
	private Date dataNasc;

	@Column(name = "doador_orgao")
	private Boolean doadorOrgao;

	@Column(name = "doador_sanguineo")
	private Boolean doadorSanguineo;

	@Column(name = "fator_rh", length = 1)
	private String fatorRh;

	@Column(name = "fator_sanguineo", length = 2)
	private String fatorSanguineo;

	@Column(name = "nome_completo", length = 100)
	private String nomeCompleto;

	@Column(name = "nome_mae", length = 100)
	private String nomeMae;

	@Column(name = "nome_pai", length = 100)
	private String nomePai;

	@Column(name = "obs_adicional", length = 255)
	private String obsAdicional;

	@Column(length = 1)
	private String sexo;

	// campos que selmo pediu (provisórios)
	@Column(name = "matricula_sgp", length = 20)
	private String matriculaSgp;

	@Column(name = "nome_guerra_sgp", length = 50)
	private String nomeGuerraSgp;

	@Column(name = "rg_militar_sgp", length = 20)
	private String rgMilitarSgp;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_ingresso_sgp")
	private Date dataIngressoSgp;

	@Column(name = "oid_ome_sgp", length = 20)
	private String oidOmeSgp;

	@Column(name = "oid_cargo_quadro_sgp", length = 20)
	private String oidCargoQuadroSgp;

	@Column(name = "oid_pessoa_sgp", length = 20)
	private String oidPessoaSgp;

	@Column(name = "pessoa_validado")
	private Boolean pessoaValidado;
	
	/*@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "naturalidade", foreignKey = @ForeignKey(name = "fkey_naturalidade") )
	private Cidade naturalidade;*/

	// campos mapeados de outras tabelas

	@Override
	public String toString() {
		return "Pessoa=" + idPessoa + ", cpf=" + cpf + ", dataNasc=" + dataNasc + ", doadorOrgao=" + doadorOrgao
				+ ", doadorSanguineo=" + doadorSanguineo + ", fatorRh=" + fatorRh + ", fatorSanguineo=" + fatorSanguineo
				+ ", nomeCompleto=" + nomeCompleto + ", nomeMae=" + nomeMae + ", nomePai=" + nomePai + ", obsAdicional="
				+ obsAdicional + ", sexo="
				+ sexo/*
						 * + ", estadoCivil=" + estadoCivil.getIdEstadoCivil() +
						 * ", escolaridade=" + escolaridade.getIdEscolaridade()
						 * + ", religiao=" + religiao.getIdReligiao()
						 */;
	}

	@Override
	public Pessoa clone() {

		try {
			return (Pessoa) super.clone();
		} catch (CloneNotSupportedException e) {
			//System.out.println("O objeto Pessoa não pôde ser clonado." + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public Integer getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getCodRef() {
		return codRef;
	}

	public void setCodRef(String codRef) {
		this.codRef = codRef;
	}

	public String getNomeFoto() {
		return nomeFoto;
	}

	public void setNomeFoto(String nomeFoto) {
		this.nomeFoto = nomeFoto;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}

	public Boolean getDoadorOrgao() {
		return doadorOrgao;
	}

	public void setDoadorOrgao(Boolean doadorOrgao) {
		this.doadorOrgao = doadorOrgao;
	}

	public Boolean getDoadorSanguineo() {
		return doadorSanguineo;
	}

	public void setDoadorSanguineo(Boolean doadorSanguineo) {
		this.doadorSanguineo = doadorSanguineo;
	}

	public String getFatorRh() {
		return fatorRh;
	}

	public void setFatorRh(String fatorRh) {
		this.fatorRh = fatorRh;
	}

	public String getFatorSanguineo() {
		return fatorSanguineo;
	}

	public void setFatorSanguineo(String fatorSanguineo) {
		this.fatorSanguineo = fatorSanguineo;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getObsAdicional() {
		return obsAdicional;
	}

	public void setObsAdicional(String obsAdicional) {
		this.obsAdicional = obsAdicional;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getMatriculaSgp() {
		return matriculaSgp;
	}

	public void setMatriculaSgp(String matriculaSgp) {
		this.matriculaSgp = matriculaSgp;
	}

	public String getNomeGuerraSgp() {
		return nomeGuerraSgp;
	}

	public void setNomeGuerraSgp(String nomeGuerraSgp) {
		this.nomeGuerraSgp = nomeGuerraSgp;
	}

	public String getRgMilitarSgp() {
		return rgMilitarSgp;
	}

	public void setRgMilitarSgp(String rgMilitarSgp) {
		this.rgMilitarSgp = rgMilitarSgp;
	}

	public Date getDataIngressoSgp() {
		return dataIngressoSgp;
	}

	public void setDataIngressoSgp(Date dataIngressoSgp) {
		this.dataIngressoSgp = dataIngressoSgp;
	}

	public String getOidOmeSgp() {
		return oidOmeSgp;
	}

	public void setOidOmeSgp(String oidOmeSgp) {
		this.oidOmeSgp = oidOmeSgp;
	}

	public String getOidCargoQuadroSgp() {
		return oidCargoQuadroSgp;
	}

	public void setOidCargoQuadroSgp(String oidCargoQuadroSgp) {
		this.oidCargoQuadroSgp = oidCargoQuadroSgp;
	}

	public String getOidPessoaSgp() {
		return oidPessoaSgp;
	}

	public void setOidPessoaSgp(String oidPessoaSgp) {
		this.oidPessoaSgp = oidPessoaSgp;
	}

	
	public Boolean getPessoaValidado() {
		return pessoaValidado;
	}

	public void setPessoaValidado(Boolean pessoaValidado) {
		this.pessoaValidado = pessoaValidado;
	}
	
/*	public Cidade getNaturalidade() {
		return naturalidade;
	}

	public void setNaturalidade(Cidade naturalidade) {
		this.naturalidade = naturalidade;
	}*/

	//	public PessoaDocumentoFuncional getPessoaDocumentoFuncional() {
//		return PessoaDocumentoFuncional;
//	}
//
//	public void setPessoaDocumentoFuncional(PessoaDocumentoFuncional pessoaDocumentoFuncional) {
//		PessoaDocumentoFuncional = pessoaDocumentoFuncional;
//	}
//	
//	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codRef == null) ? 0 : codRef.hashCode());
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((dataIngressoSgp == null) ? 0 : dataIngressoSgp.hashCode());
		result = prime * result + ((dataNasc == null) ? 0 : dataNasc.hashCode());
		result = prime * result + ((doadorOrgao == null) ? 0 : doadorOrgao.hashCode());
		result = prime * result + ((doadorSanguineo == null) ? 0 : doadorSanguineo.hashCode());
		result = prime * result + ((fatorRh == null) ? 0 : fatorRh.hashCode());
		result = prime * result + ((fatorSanguineo == null) ? 0 : fatorSanguineo.hashCode());
		result = prime * result + ((idPessoa == null) ? 0 : idPessoa.hashCode());
		result = prime * result + ((matriculaSgp == null) ? 0 : matriculaSgp.hashCode());
		result = prime * result + ((nomeCompleto == null) ? 0 : nomeCompleto.hashCode());
		result = prime * result + ((nomeFoto == null) ? 0 : nomeFoto.hashCode());
		result = prime * result + ((nomeGuerraSgp == null) ? 0 : nomeGuerraSgp.hashCode());
		result = prime * result + ((nomeMae == null) ? 0 : nomeMae.hashCode());
		result = prime * result + ((nomePai == null) ? 0 : nomePai.hashCode());
		result = prime * result + ((obsAdicional == null) ? 0 : obsAdicional.hashCode());
		result = prime * result + ((oidCargoQuadroSgp == null) ? 0 : oidCargoQuadroSgp.hashCode());
		result = prime * result + ((oidOmeSgp == null) ? 0 : oidOmeSgp.hashCode());
		result = prime * result + ((oidPessoaSgp == null) ? 0 : oidPessoaSgp.hashCode());
		result = prime * result + ((pessoaValidado == null) ? 0 : pessoaValidado.hashCode());
		result = prime * result + ((rgMilitarSgp == null) ? 0 : rgMilitarSgp.hashCode());
		result = prime * result + ((sexo == null) ? 0 : sexo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (codRef == null) {
			if (other.codRef != null)
				return false;
		} else if (!codRef.equals(other.codRef))
			return false;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (dataIngressoSgp == null) {
			if (other.dataIngressoSgp != null)
				return false;
		} else if (!dataIngressoSgp.equals(other.dataIngressoSgp))
			return false;
		if (dataNasc == null) {
			if (other.dataNasc != null)
				return false;
		} else if (!dataNasc.equals(other.dataNasc))
			return false;
		if (doadorOrgao == null) {
			if (other.doadorOrgao != null)
				return false;
		} else if (!doadorOrgao.equals(other.doadorOrgao))
			return false;
		if (doadorSanguineo == null) {
			if (other.doadorSanguineo != null)
				return false;
		} else if (!doadorSanguineo.equals(other.doadorSanguineo))
			return false;
		if (fatorRh == null) {
			if (other.fatorRh != null)
				return false;
		} else if (!fatorRh.equals(other.fatorRh))
			return false;
		if (fatorSanguineo == null) {
			if (other.fatorSanguineo != null)
				return false;
		} else if (!fatorSanguineo.equals(other.fatorSanguineo))
			return false;
		if (idPessoa == null) {
			if (other.idPessoa != null)
				return false;
		} else if (!idPessoa.equals(other.idPessoa))
			return false;
		if (matriculaSgp == null) {
			if (other.matriculaSgp != null)
				return false;
		} else if (!matriculaSgp.equals(other.matriculaSgp))
			return false;
		if (nomeCompleto == null) {
			if (other.nomeCompleto != null)
				return false;
		} else if (!nomeCompleto.equals(other.nomeCompleto))
			return false;
		if (nomeFoto == null) {
			if (other.nomeFoto != null)
				return false;
		} else if (!nomeFoto.equals(other.nomeFoto))
			return false;
		if (nomeGuerraSgp == null) {
			if (other.nomeGuerraSgp != null)
				return false;
		} else if (!nomeGuerraSgp.equals(other.nomeGuerraSgp))
			return false;
		if (nomeMae == null) {
			if (other.nomeMae != null)
				return false;
		} else if (!nomeMae.equals(other.nomeMae))
			return false;
		if (nomePai == null) {
			if (other.nomePai != null)
				return false;
		} else if (!nomePai.equals(other.nomePai))
			return false;
		if (obsAdicional == null) {
			if (other.obsAdicional != null)
				return false;
		} else if (!obsAdicional.equals(other.obsAdicional))
			return false;
		if (oidCargoQuadroSgp == null) {
			if (other.oidCargoQuadroSgp != null)
				return false;
		} else if (!oidCargoQuadroSgp.equals(other.oidCargoQuadroSgp))
			return false;
		if (oidOmeSgp == null) {
			if (other.oidOmeSgp != null)
				return false;
		} else if (!oidOmeSgp.equals(other.oidOmeSgp))
			return false;
		if (oidPessoaSgp == null) {
			if (other.oidPessoaSgp != null)
				return false;
		} else if (!oidPessoaSgp.equals(other.oidPessoaSgp))
			return false;
		if (pessoaValidado == null) {
			if (other.pessoaValidado != null)
				return false;
		} else if (!pessoaValidado.equals(other.pessoaValidado))
			return false;
		if (rgMilitarSgp == null) {
			if (other.rgMilitarSgp != null)
				return false;
		} else if (!rgMilitarSgp.equals(other.rgMilitarSgp))
			return false;
		if (sexo == null) {
			if (other.sexo != null)
				return false;
		} else if (!sexo.equals(other.sexo))
			return false;
		return true;
	}

}
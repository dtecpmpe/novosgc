package br.gov.pe.pm.sgc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NaturalId;

import br.gov.pe.pm.sgc.rn.OmeRn;

@Entity
@Table(name = "usuario", schema = "conta")
public class Usuario implements Serializable, Cloneable {

	private static final long serialVersionUID = 2496373653811231032L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario", nullable = false)
	private Integer idUsuario;

	@Column(name = "nome", length = 100)
	private String nome;

	@Column(name = "email", length = 255)
	private String email;

	@NaturalId
	@Column(name = "login", length = 20)
	private String login;

	@Column(name = "senha", length = 255)
	private String senha;

	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "token")
	private String token;

	@Column(name = "token_exp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tokenExp;

	@Column(name = "perfil", length = 30)
	private String perfil;
	
	@Column(name = "id_organizacao")
	private Integer organizacao;

	/*
	 * @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	 * 
	 * @OnDelete(action = OnDeleteAction.NO_ACTION)
	 * 
	 * @JoinColumn(name = "id_organizacao", nullable = false, foreignKey
	 * = @ForeignKey(name = "usuario_id_organizacao_fkey")) private Organizacao
	 * organizacao;
	 */

	// construtor
	/*
	 * public Usuario() { this.organizacao = new Organizacao(); }
	 */

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getTokenExp() {
		return tokenExp;
	}

	public void setTokenExp(Date tokenExp) {
		this.tokenExp = tokenExp;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	

	/*
	 * public Organizacao getOrganizacao() { return organizacao; }
	 * 
	 * public void setOrganizacao(Organizacao organizacao) { this.organizacao =
	 * organizacao; }
	 */

	public Integer getOrganizacao() {
		return organizacao;
	}

	public void setOrganizacao(Integer organizacao) {
		this.organizacao = organizacao;
	}

	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", nome=" + nome + ", email=" + email + ", login=" + login
				+ ", senha=" + senha + ", ativo=" + ativo + ", token=" + token + ", tokenExp=" + tokenExp + ", perfil="
				+ perfil + "]";
	}

	@Override
	public Usuario clone() {
		try {
			return (Usuario) super.clone();
		} catch (CloneNotSupportedException e) {
			//System.out.println("O objeto Usuario não pode ser clonado." + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ativo ? 1231 : 1237);
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((idUsuario == null) ? 0 : idUsuario.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		/*
		 * result = prime * result + ((organizacao == null) ? 0 :
		 * organizacao.hashCode());
		 */
		result = prime * result + ((perfil == null) ? 0 : perfil.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((tokenExp == null) ? 0 : tokenExp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (ativo != other.ativo)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (idUsuario == null) {
			if (other.idUsuario != null)
				return false;
		} else if (!idUsuario.equals(other.idUsuario))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		/*
		 * if (organizacao == null) { if (other.organizacao != null) return false; }
		 * else if (!organizacao.equals(other.organizacao)) return false;
		 */
		if (perfil == null) {
			if (other.perfil != null)
				return false;
		} else if (!perfil.equals(other.perfil))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		if (tokenExp == null) {
			if (other.tokenExp != null)
				return false;
		} else if (!tokenExp.equals(other.tokenExp))
			return false;
		return true;
	}
	

	public String getOme() {
		return new OmeRn().carregar(organizacao).getSigla();
	}

}

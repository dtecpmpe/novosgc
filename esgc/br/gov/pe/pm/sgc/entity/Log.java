package br.gov.pe.pm.sgc.entity;

import java.io.Serializable; 
import java.util.Date;
  
import javax.persistence.Column; 
import javax.persistence.Entity; 
import javax.persistence.EnumType; 
import javax.persistence.Enumerated; 
import javax.persistence.GeneratedValue; 
import javax.persistence.GenerationType;
import javax.persistence.Id; 
import javax.persistence.Table;
import br.gov.pe.pm.sgc.entity.enumAux.AcaoLogEnum;
  
  
@Entity  
@Table(name = "log", schema = "conta") 
public class Log implements Serializable {
  
  private static final long serialVersionUID = -2666237170027109304L;
  
  // VARIÁVEIS
  
  @Id  
  @GeneratedValue(strategy = GenerationType.IDENTITY)  
  @Column(name = "id_log", nullable = false) 
  private Long idLog;
  
  @Column(name = "nome_tabela", length = 45) 
  private String nomeTabela;
  
  @Enumerated(EnumType.STRING)  
  @Column(length = 1, nullable = false, columnDefinition = "char") 
  private AcaoLogEnum acao;
  
  @Column(name = "valor_antigo", columnDefinition = "text") 
  private String valorAntigo;
  
  @Column(name = "valor_novo", columnDefinition = "text") 
  private String valorNovo;
  
  @Column(name = "data_alteracao") 
  private Date dataAlteracao;
  
  @Column(length = 15) 
  private String usuario;
  
  //CONSTRUTOR 
  public Log() { }
  
  // GETTERS E SETTERS public String getNomeTabela() { return nomeTabela; }
  
  public void setNomeTabela(String nomeTabela) { 
	  this.nomeTabela = nomeTabela;
  }
  
  public AcaoLogEnum getAcao() { 
	  return acao; 
  }
  
  public void setAcao(AcaoLogEnum acao) { 
	  this.acao = acao; 
  }
  
  public String getValorAntigo() { 
	  return valorAntigo; 
  }
  
  public void setValorAntigo(String valorAntigo) { 
	  this.valorAntigo =  valorAntigo; 
  }
  
  public String getValorNovo() { 
	  return valorNovo; 
  }
  
  public void setValorNovo(String valorNovo) { 
	  this.valorNovo = valorNovo; 
  }
  
  public Date getDataAlteracao() { 
	  return dataAlteracao; 
  }
  
  public void setDataAlteracao(Date dataAlteracao) { 
	  this.dataAlteracao = dataAlteracao; 
  }
  
  public String getUsuario() { 
	  return usuario; 
  }
  
  public void setUsuario(String usuario) { 
	  this.usuario = usuario; 
  }

  @Override
  public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((acao == null) ? 0 : acao.hashCode());
	result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
	result = prime * result + ((idLog == null) ? 0 : idLog.hashCode());
	result = prime * result + ((nomeTabela == null) ? 0 : nomeTabela.hashCode());
	result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
	result = prime * result + ((valorAntigo == null) ? 0 : valorAntigo.hashCode());
	result = prime * result + ((valorNovo == null) ? 0 : valorNovo.hashCode());
	return result;
  }

  @Override
  public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Log other = (Log) obj;
	if (acao != other.acao)
		return false;
	if (dataAlteracao == null) {
		if (other.dataAlteracao != null)
			return false;
	} else if (!dataAlteracao.equals(other.dataAlteracao))
		return false;
	if (idLog == null) {
		if (other.idLog != null)
			return false;
	} else if (!idLog.equals(other.idLog))
		return false;
	if (nomeTabela == null) {
		if (other.nomeTabela != null)
			return false;
	} else if (!nomeTabela.equals(other.nomeTabela))
		return false;
	if (usuario == null) {
		if (other.usuario != null)
			return false;
	} else if (!usuario.equals(other.usuario))
		return false;
	if (valorAntigo == null) {
		if (other.valorAntigo != null)
			return false;
	} else if (!valorAntigo.equals(other.valorAntigo))
		return false;
	if (valorNovo == null) {
		if (other.valorNovo != null)
			return false;
	} else if (!valorNovo.equals(other.valorNovo))
		return false;
	return true;
	}
  
}
 
package br.gov.pe.pm.sgc.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import br.gov.pe.pm.sgc.entity.Usuario;
import br.gov.pe.pm.sgc.rn.UsuarioRn;

@Named("mainBean")

@SessionScoped
public class MainBean implements Serializable {

	private static final long serialVersionUID = 5700494727689904778L;
	private Usuario usuario;

	public MainBean() {
		String login = (((SecurityContext) SecurityContextHolder.getContext()).getAuthentication().getName());
		this.usuario = new UsuarioRn().buscarPorLogin(login);

	}

	// metodos
	public Usuario getUsuario() {
		return usuario;
	}

	// metodos public Usuario getUsuario() { return usuario; }

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}

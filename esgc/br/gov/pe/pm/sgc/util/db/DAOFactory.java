package br.gov.pe.pm.sgc.util.db;

import br.gov.pe.pm.sgc.interfaceDAO.Dao;

public class DAOFactory {
	
	public static Dao criarDAO(Object o) {
		Dao dao = (Dao) o;
		dao.setSession(HibernateUtil.getSessionFactory().getCurrentSession());
		return dao;
	}
	
}
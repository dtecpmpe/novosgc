package br.gov.pe.pm.sgc.util.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public class FormataTextoConverterUtil implements Converter {

	//Formata o texto do campo(Valor Novo) da cons_auditoria

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		String formataTexto = (String) value;
		if (value != null && !value.equals(""))
			formataTexto = ((String) value).replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\"", "");
		return formataTexto;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		// TODO Auto-generated method stub
		return null;
	}


}
